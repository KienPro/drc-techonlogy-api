# Getting started

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/6.x#installation)

Clone the repository

    git clone git@gitlab.com:quick-app/quick-app-api.git

Switch to the repo folder

    cd quick-app-api

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

**TL;DR command list**

    git clone git@gitlab.com:quick-app/quick-app-api.git
    cd quick-app-api
    composer install
    cp .env.example .env
    php artisan key:generate
    
**Make sure you set the correct database connection information before running the migrations** [Environment variables](#environment-variables)

    php artisan migrate
    php artisan serve

## Database seeding

**Populate the database with seed data with relationships which includes users, roles, permissions. This can help you to quickly start testing the api or couple a frontend and start using it with ready content.**

Open the UserRolePermissionTableSeeder and set the property values as per your requirement

    database/seeds/UserRolePermissionTableSeeder.php

Run the database seeder and you're done

    php artisan db:seed --class UserRolePermissionTableSeeder

***Note*** : It's recommended to have a clean database before seeding. You can refresh your migrations at any point to clean the database by running the following command

    php artisan migrate:refresh

## Environment variables

- `.env` - Environment variables can be set in this file

***Note*** : You can quickly set the database information and other variables in this file and have the application fully working.

----------

# Testing API

Run the laravel development server

    php artisan serve --port 8081

    or

    ./serve-run

The api can now be accessed at

    http://localhost:8081
