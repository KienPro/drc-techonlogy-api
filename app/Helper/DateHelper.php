<?php
namespace App\Helper;

use Carbon\Carbon;

class DateHelper
{
    public static function getNow()
    {
		return Carbon::now();
	}

    public static function getDateTimeString()
    {
		return self::getNow()->toDateTimeString();
	}

	public static function getMobileDateTimeString($date_time)
    {
        if(!$date_time) return null;
        return Carbon::parse($date_time)->format(config('format.clock.mobile.date_time'));
    }

    public static function getMobileDateString($date)
    {
        if(!$date) return null;
        return Carbon::parse($date)->format(config('format.clock.mobile.date'));
    }

    public static function getMobileTimeString($time)
    {
        if(!$time) return null;
        return Carbon::parse($time)->format(config('format.clock.mobile.time'));
    }

    public static function getTimeStampFromDate($date)
    {
        if(!$date) return null;
        return Carbon::parse($date)->timestamp;
    }

    public static function formatDateTimeAdmin($date, $format = 'Y-m-d H:i:s')
    {
        if(!$date) return null;
        return Carbon::parse($date)->format($format);
    }

    public static function formatDateAdmin($date, $format = 'Y-m-d')
    {
        if(!$date) return null;
        return Carbon::parse($date)->format($format);
    }

    public static function formatTime($time, $format = 'H:i')
    {
        if(!$time) return null;
        return Carbon::parse($time)->format($format);
    }
}
