<?php

namespace App\Helper;

use Log;
use Exception;
use Illuminate\Support\Facades\Mail;

class MailHelper
{
	public static function sendMailUsers($template, $title, $data, $users, $sender = null)
    {
		$data['sender'] = $sender ?? config('mail.sender');

		foreach($users as $user)
		{
			$data['name'] = $user['name'];
			$data['email'] = $user['email'];
			try{
                Mail::send($template, $data, function($message) use ($data, $title)
                {
                    $message->to($data['email'], $data['name'])->subject($title);
				});
            } catch (Exception $e){
                report($e);
                Log::info('send mail: ',['error'  => $e->getMessage()]);
            }
		}
		return true;
	}
}
