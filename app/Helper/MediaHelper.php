<?php
namespace App\Helper;

use App\Models\Media;
use Storage;

class MediaHelper
{
    public static function storeImageBase64($data, $type = 'png', $name = null)
    {
        $file_name = $name;
        if($file_name) $file_name .= '-';

        $file_name .= md5(microtime()) . '.' . $type;

        if(config('media.is_aws_s3'))
            Storage::disk('s3')->put('uploads/images/' . $file_name, base64_decode(explode(',', $data)[1] ?? ''), 'public');
        else
            file_put_contents(public_path('uploads/') .  'images/' . $file_name, base64_decode(explode(',', $data)[1] ?? ''));

        $media = Media::create([
            'url'	=> '/uploads/images/'. $file_name,
            'name'	=> $name ?? $file_name,
        ]);

        return $media->id;
    }

    public static function storeFileBase64($data, $ext, $name = null)
    {
        $file_name = $name;
        if($file_name) $file_name .= '-';

        $file_name .= md5(microtime()) . '.' . $ext;

        if(config('media.is_aws_s3'))
            Storage::disk('s3')->put('uploads/original/' . $file_name, base64_decode(explode(',', $data)[1] ?? ''), 'public');
        else
            file_put_contents(public_path('uploads/') . 'original/' . $file_name, base64_decode(explode(',', $data)[1] ?? ''));

        $media = Media::create([
            'url'	=> '/uploads/original/'. $file_name,
            'name'	=> $name ?? $file_name,
        ]);

        return $media->id;
    }
}
