<?php

namespace App\Helper;

use App\Models\Customer;
use App\Models\Notification;
use App\Models\NotificationUser;
use App\Models\User;
use App\Models\OnesignalUser;
use DB;
use Log;

class NotificationHelper
{
    public static function request($user_type, $filters, $data, $include_player_ids = null)
    {
        $fields = array(
            'app_id' => config('broadcasting.onesignal.'. $user_type .'.app_id'),
            'filters' => $filters,
            'data' => $data,
            'contents' => $data['message'],
            'ios_badgeCount' => 1,
            'ios_badgeType' => 'Increase',
            'small_icon' => 'ic_noti_logo',
            'headings' => $data['title'],
            'content_available' => 1
        );

        $fields['url'] = (empty($data['url']) ? '' : $data['url']);

        if($include_player_ids) {
            $fields['include_player_ids'] = $include_player_ids;
        }

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, config('broadcasting.onesignal.app_url'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ' . config('broadcasting.onesignal.'. $user_type .'.app_key'),));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);

        Log::info("One Signal Response", [
            'response' => $response,
        ]);

        curl_close($ch);
    }

    public static function setAndRequest($user_type, $data, $filter, $include_player_ids = null) 
    {
        $temp_data = $data;

        if($user_type == 'customer') {
            $temp_data['message'] = null;
            $temp_data['message'] = [
                'en' => $data['message']['kh'] ??  $data['message']
            ];
            $temp_data['title'] = null;
            $temp_data['title'] = [
                'en' => $data['title']['kh'] ??  $data['title']
            ];
            self::request($user_type, $filter, $temp_data, $include_player_ids);

            $filter[0]['value'] = 'en';
            $temp_data['message'] = null;
            $temp_data['message'] = [
                'en' => $data['message']['en'] ??  $data['message']
            ];
            $temp_data['title'] = null;
            $temp_data['title'] = [
                'en' => $data['title']['en'] ??  $data['title']
            ];
        }

        self::request($user_type, $filter, $temp_data, $include_player_ids);
    }

    public static function send($user_type, $user_ids, &$data)
    {
        if(!in_array($user_type, ['user', 'customer'])) return false;

        $user_ids = array_unique($user_ids);

        $filter = [];

        if($user_type == 'customer') {
            $filter[] = [
                'field' => 'tag',
                'key' => 'language',
                'relation' => '=',
                'value' => 'km'
            ];

            $filter[] = [
                "operator" => "AND",
            ];
        }

        foreach ($user_ids as $id) {
            $temp_filter = $filter;

            if($id != null) {
                $temp_filter[] = [
                    'field' => 'tag',
                    'key' => 'id',
                    'relation' => '=',
                    'value' => $id
                ];
            }

            self::setAndRequest($user_type, $data, $temp_filter);
        }

        return true;
    }

    public static function create($user_type, $user_ids, &$data, $type = null, $type_id = null)
    {
        switch ($user_type) {
            case 'user':
                $user_class = User::class;
                break;
            case 'customer':
                $user_class = Customer::class;
                break;
        }

        if(!isset($user_class)) return false;

        $user_ids = array_unique($user_ids);

        $notification = Notification::create([
            'title_en' => $data['title']['en'] ?? (is_string($data['title']) ? $data['title'] : ''),
            'title_kh' => $data['title']['kh'] ?? (is_string($data['title']) ? $data['title'] : ''),
            'message_en' => $data['message']['en'] ?? (is_string($data['message']) ? $data['message'] : ''),
            'message_kh' => $data['message']['kh'] ?? (is_string($data['message']) ? $data['message'] : ''),
            'data' => json_encode($data),
            'type' => $type,
            'type_id' => $type_id
        ]);

        if($user_type == 'operator') {
            $data['url'] = config('operator.notification_url') . '?read=' . $notification->id;
        }

        foreach ($user_ids as $user_id) {
            if($user_id == null) {
                NotificationUser::createForAllUser([
                    'notification_id' => $notification->id,
                    'user_type' => $user_class
                ]);
                break;
            }

            NotificationUser::create([
                'notification_id' => $notification->id,
                'user_id' => $user_id,
                'user_type' => $user_class
            ]);
        }

        return true;
    }

    public static function createAndSend($user_type, $user_ids, $data, $type = null, $type_id = null)
    {
        self::create($user_type, $user_ids, $data, $type, $type_id);

        self::send($user_type, $user_ids, $data);
    }

    public static function toCustomerByType($data, $type, $type_id = null)
    {
        $setting_type = 'notification_' . $type;

        $onesignal_users = OnesignalUser::where('user_type', Customer::class)
                                    ->where(function($q) use($setting_type) {
                                        $q->where('user_id', null)
                                            ->OrWhereHas('customer_settings', function($q) use($setting_type) {
                                                $q->where(function($q) use($setting_type) {
                                                    $q->where('setting_type', $setting_type)
                                                        ->where('value', 1);
                                                });
                                            });
                                    })
                                    ->get();

        $player_ids = null;
        $customer_ids = [];
        if(count($onesignal_users)) {
            $player_ids = $onesignal_users->where('player_id', '<>', null)->pluck('player_id')->toArray();
    
            $customer_ids = $onesignal_users->where('user_id', '<>', null)->pluck('user_id')->toArray();
        }

        if(count($customer_ids) == 0) {
            $customer_ids = Customer::doesntHave('onesignal_user')->pluck('id')->toArray();
        }

        if(count($customer_ids)) {
            self::create('customer', $customer_ids, $data, $type, $type_id);
        }
        
        self::setAndRequest('customer', $data, [], $player_ids);
    }
}
