<?php

function get_message($key, $word = 'not_found') {
    return __('messages.' . $word, ['attribute' => ucwords(get_validation_attribute($key))]);
}

function get_validation_attribute($key) {
    if(__('validation.attributes.' . $key) == 'validation.attributes.' . $key) {
        return $key;
    }

    return __('validation.attributes.' . $key);
}

function ok($data = null)
{
    return response()->json(['success' => true, 'data' => $data]);
}

function fail($message, $code = 400)
{
    return response()->json(["success" => false, "message" => $message], $code);
}

function new_fail($message, $code = 400)
{
    return response()->json($message, $code);
}

function new_ok($data = null)
{
    return response()->json($data);
}

function mapArray($array, $keys)
{
    $data = [];

    foreach($keys as $key)
    {
        $data[$key] = $array[$key] ?? null;
    }

    return $data;
}


if (!function_exists('random4digit')) {
    /**
     * @return string
     */
    function random4digit(): string
    {
        return rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);
    }
}

if (!function_exists('moveItemToEnd')) {
    /**
     * @return Collection
     */
    function moveItemCollectionToEnd($collection, $key, $value)
    {
        $match_item = null;
        
        $collection = $collection->filter(function($item) use ($key, $value, &$match_item) {
            if($is = $item[$key] == $value) {
                $match_item = $item;
            }

            return $is;
        });

        if(!$match_item) {
            return $collection;
        }

        return $collection->push($match_item);
    }
}

if (!function_exists('moveItemToStart')) {
    /**
     * @return Collection
     */
    function moveItemCollectionToStart($collection, $key, $value)
    {
        $match_item = null;
        
        $collection = $collection->filter(function($item) use ($key, $value, &$match_item) {
            if($is = $item[$key] == $value) {
                $match_item = $item;
            }

            return $is;
        });

        if(!$match_item) {
            return $collection;
        }
        
        return $collection->prepend($match_item);
    }
}