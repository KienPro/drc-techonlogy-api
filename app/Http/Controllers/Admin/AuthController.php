<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Auth\AdminLoginRequest;
use App\Http\Requests\Admin\Auth\AdminForgotPasswordResetRequest;
use App\Models\AdminForgetToken;
use App\Models\User;
use Illuminate\Http\Request;
use DB;
use Exception;

class AuthController extends Controller
{
    public function login(AdminLoginRequest $request)
    {
        $auth = User::login($request);

        if(!$auth) return fail('Invalid Email or Password.');

        return ok($auth);
    }

    public function logout()
    {
        return ok();
    }

    public function forgotPassword(Request $request)
    {
        DB::beginTransaction();
        $user = User::where('email', $request->email)->first();

        if (!$user)
            return fail('Invalid email address.');

        AdminForgetToken::generateForgetCode($user);

        DB::commit();
        return ok();
    }

    public function forgotPasswordVerify(Request $request)
    {
        DB::beginTransaction();
        $token = AdminForgetToken::verifyCode($request->email, $request->verify_code);

        if(!$token)
            return fail(__('messages.invalid.verify_code'));

        DB::commit();
        return ok(['token' => $token]);
    }

    public function forgotPasswordReset(AdminForgotPasswordResetRequest $request)
    {
        DB::beginTransaction();
        if(!AdminForgetToken::verifyForgetToken($request->token, $request->email)) {
            return fail(__('messages.can_not.reset_password'));
        }

        $user = User::active()->where('email', $request->email)->first();

        if(!$user) {
            return fail(__('messages.can_not.reset_password'));
        }

        $user->update(['password' => bcrypt($request->password)]);

        DB::commit();
        return ok(__('messages.reset_password_successfully'));
    }

    public function getUserPer()
    {
        $data = [];

        $roles = auth('web')->user()->roles;

        $roles->load('permissions.module', 'permissions.action');

        foreach ($roles as $role)
        {
            foreach ($role->permissions as $permission)
            {
                $data[$permission->slug] = true;
            }
        }

        return ok($data);
    }
}
