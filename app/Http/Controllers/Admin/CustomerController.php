<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\General\StatusUpdateRequest;
use App\Http\Requests\Customer\AdminCustomerStoreOrModifiedRequest;
use App\Models\Customer;

class CustomerController extends Controller
{
    public function list()
    {
        $this->getListingParams();
        $this->addMoreParams(['platform', 'customer_group_id']);

        return ok(Customer::listAdmin($this->params));
    }

    public function listAll()
    {
        return ok(
            Customer::orderBy('name', 'asc')->get(['id', 'name'])
        );
    }

    public function create(AdminCustomerStoreOrModifiedRequest $request)
    {
        $customer = Customer::store($request);

        return ok($customer->only('id', 'name'));
    }

    public function show($id)
    {
        $item = Customer::find($id);

        if(!$item)
            return fail('Customer not found.');

        $item->load('media');

        return ok($item->only('id',  'name',  'customer_group_id', 'phone_number', 'other_phone', 'email', 'address', 'enable_status', 'media'));
    }

    public function update($id, AdminCustomerStoreOrModifiedRequest $request)
    {
        $item = Customer::change($id, $request);

        if(!$item)
            return fail('Customer not found.');

        return ok();
    }

    public function status($id, StatusUpdateRequest $request)
    {
        $item = Customer::find($id);

        if(!$item)
            return fail('Customer not found.');

        $item->update($request->only('enable_status'));

        return ok();
    }

    public function delete($id)
    {
        $item = Customer::find($id);

        if(!$item)
            return fail('Customer not found.');

        $item->delete();

        return ok();
    }
}
