<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Customer;
use App\Models\User;
use App\Models\Order;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function index($selectDate = null)
    {
        $months = [];
        $daily_revenues = Order::select('total', 'created_at')->whereRaw('YEAR(created_at)', now()->format('Y'))->get()
            ->groupBy(function ($query){
                return Carbon::parse($query->created_at)->format('F');
            })
            ->map(function ($items, $month) use (&$months){
                $months[] = $month;
                $daysInMonth = $items[0]->created_at->daysInMonth;
                $return = array_fill(0, $daysInMonth - 1, 0);

                $items = $items->groupBy(function ($query){
                    return Carbon::parse($query->created_at)->format('d');
                });

                foreach ($items as $key => $item) {
                    $return[intval($key) - 1] = $item->sum('total');
                }

                return [
                    'months' => $months,
                    'data' => $return,
                    'labels' => range(1, $daysInMonth)
                ];
            });

        // $year =  Carbon::parse($selectDate)->format('Y');
        // $month = Carbon::parse($selectDate)->format('m');

        // $daysInMonth = Carbon::parse($year . '-' . $month)->daysInMonth;
        // $daily_revenues = array_fill(0, $daysInMonth, 0); 
        // Transaction::selectRaw('DATE_FORMAT(check_out_at, "%d") as date,sum(total) as daily_revenues')
        //             ->whereNotNull('check_out_at')
        //             ->whereRaw('year(check_out_at) = ' . $year)
        //             ->whereRaw('month(check_out_at) = ' . $month)
        //             ->groupBy('date')
        //             ->get()
        //             ->map(function($item) use(&$daily_revenues) {
        //                 $daily_revenues[$item->date - 1] = $item->daily_revenues;
        //             });
        return ok([
            'total_pending_orders' => Order::where('status', 'pending')->count(),
            'total_products' => Product::count(),
            'total_customers' => Customer::count(),
            'total_users' => User::count(),
            // 'days_in_month' => $daysInMonth,
            'daily_revenues' => $daily_revenues
        ]);
    }
}
