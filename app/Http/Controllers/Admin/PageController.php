<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\General\StatusUpdateRequest;
use App\Http\Requests\Page\AdminPageStoreRequest;
use App\Http\Requests\Page\AdminPageUpdateRequest;
use App\Models\Page;
use DB;
use Exception;

class PageController extends Controller
{
    public function list()
    {
        try {
            $this->getListingParams();

            return ok(Page::listAdmin($this->params));
        } catch (Exception $e) {
            report($e);
            return fail($e->getMessage(), 500);
        }
    }

    public function create(AdminPageStoreRequest $request)
    {
        try {
            DB::BeginTransaction();

            Page::createAdmin($request);

            DB::commit();
            return ok();
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return fail($e->getMessage(), 500);
        }
    }

    public function show($id)
    {
        try {
            $page = Page::with('media')->find($id);

            if(!$page) {
                return fail('Page not found.');
            }

            return ok($page->only('id', 'title_en', 'title_kh', 'short_description_en', 'short_description_kh', 'enable_status') + ['media' => $page->media]);
        } catch (Exception $e) {
            report($e);
            return fail($e->getMessage(), 500);
        }
    }

    public function update($id, AdminPageUpdateRequest $request)
    {
        try {
            DB::BeginTransaction();

            $page = Page::updateAdmin($id, $request);

            if(!$page) {
                return fail('Page not found.');
            }

            DB::commit();
            return ok();
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return fail($e->getMessage(), 500);
        }
    }

    public function status($id, StatusUpdateRequest $request)
    {
        try {
            DB::beginTransaction();
            $page = Page::find($id);

            if(!$page) {
                return fail('Page not found.');
            }

            $page->update($request->only('enable_status'));

            DB::commit();
            return ok();
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            return fail($e->getMessage(), 500);
        }
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $page = Page::find($id);

            if(!$page) {
                return fail('Page not found.');
            }

            $page->delete();

            DB::commit();
            return ok();
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            return fail($e->getMessage(), 500);
        }
    }
}
