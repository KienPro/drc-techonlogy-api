<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Models\Permission;
use App\Http\Controllers\Controller;

class PermissionController extends Controller
{
    public function listAll()
    {
        try{
            $permissions = Permission::with('module', 'action')->get()->sortBy('module.sort')->sortBy('id')->groupBy('module.slug');

            $data = [];
            foreach ($permissions as $key => $permission) {
                $data[$key] = $permission->map(function ($item) {
                    return [
                        'id' => $item->id,
                        'action' => $item->action->slug
                    ];
                });
            }

            return ok($data);
        }catch(Exception $e) {
            report($e);
            return fail($e->getMessage(), 500);
        }
    }
}
