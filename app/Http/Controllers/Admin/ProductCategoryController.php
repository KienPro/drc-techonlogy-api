<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductCategory\AdminProductCategoryStoreOrModifiedRequest;
use App\Models\ProductCategory;

class ProductCategoryController extends Controller
{
    public function list()
    {
        $this->getListingParams('sequence', 'asc');

        return ok(ProductCategory::listAdmin($this->params));
    }

    public function listAll()
    {
        return ok(
            ProductCategory::orderBy('sequence', 'asc')
            // ->whereHas('products', function ($query) {
            //     $query->active();
            // })
            ->get(['id', 'name'])
            ->map(function($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->name,
                ];
            })
        );
    }

    public function create(AdminProductCategoryStoreOrModifiedRequest $request)
    {
        $ProductCategory = ProductCategory::store($request);

        return ok();
    }

    public function show($id)
    {
        $item = ProductCategory::find($id);

        if(!$item)
            return fail('Product Category not found.');

        return ok($item->only('id', 'name', 'sequence'));
    }

    public function update($id, AdminProductCategoryStoreOrModifiedRequest $request)
    {
        $item = ProductCategory::store($request, $id);

        if(!$item)
            return fail('Product Category not found.');

        return ok();
    }

    public function delete($id)
    {
        $item = ProductCategory::find($id);

        if(!$item)
            return fail('Product Category not found.');

        $item->delete();

        return ok();
    }
}
