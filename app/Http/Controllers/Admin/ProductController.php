<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Product\AdminProductStoreOrModifiedRequest;

use App\Models\Product;

class ProductController extends Controller
{
    public function list()
    {
        $this->getListingParams();
        $this->params['product_model_id'] = request('product_model_id');
        $this->params['product_category_id'] = request('product_category_id');
        $this->params['productt_tag_id'] = request('productt_tag_id');
        $this->params['enable_status'] = request('enable_status');
        $this->params['is_new'] = request('is_new');
        $this->params['is_bestseller'] = request('is_bestseller');

        $data = Product::listAdmin($this->params);

        $data['list']->each->append('string_tags');

        return ok($data);
    }    

    public function create(AdminProductStoreOrModifiedRequest $request)
    {
        $Product = Product::store($request);

        return ok();
    }

    public function show($id)
    {
        $item = Product::find($id);

        if(!$item)
            return fail('Product not found.');

        $item->load('media');

        return ok($item->only(
            'id', 
            'product_model_id',
            'product_category_id',
            'name',
            'short_description',
            'long_description',
            'specification',
            'enable_status',
            'sequence',
            'cost',
            'old_price',
            'current_price',
            'is_new',
            'is_bestseller',
            'tag_ids',
            'media',
            'secondary_media',
        ));
    }

    public function update($id, AdminProductStoreOrModifiedRequest $request)
    {
        $item = Product::change($request, $id);

        if(!$item)
            return fail('Product not found.');

        return ok();
    }

    public function status($id, Request $request)
    {
        $item = Product::find($id);

        if(!$item)
            return fail('Product not found.');

        $item->update($request->only('is_bestseller'));

        return ok();
    }

    public function delete($id)
    {
        $item = Product::find($id);

        if(!$item)
            return fail('Product not found.');

        $item->delete();

        return ok();
    }
}
