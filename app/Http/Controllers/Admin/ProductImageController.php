<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\General\SequenceUpdateRequest;
use App\Http\Requests\ProductImage\AdminProductImageModifiedRequest;
use App\Http\Requests\ProductImage\AdminProductImageStoreRequest;
use App\Models\ProductImage;
use App\Models\Product;

class ProductImageController extends Controller
{
    public function list()
    {
        $this->getListingParams();
        $this->params['product_id'] = request('product_id');

        $product = Product::find(request('product_id'));
        
        if(!$product)
            return fail('Product not found.');

        $data = ProductImage::listAdmin($this->params);

        return ok($data + [
            'product' => $product->only('id', 'name') +[
                'media' => $product->media,
            ]
        ]);
    }

    public function store(AdminProductImageStoreRequest $request)
    {
        ProductImage::store($request);

        return ok();
    }

    public function show($id)
    {
        $item = ProductImage::find($id);

        if(!$item)
            return fail('Product Image not found.');

        $item->load('media');

        return ok($item->only('id', 'product_id', 'sequence', 'media'));
    }

    public function update($id, AdminProductImageModifiedRequest $request)
    {
        $item = ProductImage::change($request, $id);

        if(!$item)
            return fail('Product Image not found.');

        return ok();
    }

    public function sequence($id, SequenceUpdateRequest $request)
    {
        $item = ProductImage::find($id);

        if(!$item)
            return fail('Product Image not found.');

        $item->update($request->only('sequence'));

        return ok();
    }

    public function delete($id)
    {
        $item = ProductImage::find($id);

        if(!$item)
            return fail('Product Image not found.');

        $item->delete();

        return ok();
    }
}
