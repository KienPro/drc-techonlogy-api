<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductModel\AdminProductModelStoreOrModifiedRequest;
use App\Models\ProductModel;

class ProductModelController extends Controller
{
    public function list()
    {
        $this->getListingParams();

        return ok(ProductModel::listAdmin($this->params));
    }

    public function listAll()
    {
        return ok(
            ProductModel::
            // whereHas('products', function ($query) {
            //     $query->active();
            // })
            // ->
            get(['id', 'name', 'product_category_id'])
            ->map(function($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->name,
                    'product_category_id' => $item->product_category_id,
                ];
            })
        );
    }

    public function create(AdminProductModelStoreOrModifiedRequest $request)
    {
        $ProductModel = ProductModel::store($request);

        return ok();
    }

    public function show($id)
    {
        $item = ProductModel::find($id);

        if(!$item)
            return fail('Product Model not found.');

        return ok($item->only('id', 'product_category_id','name'));
    }

    public function update($id, AdminProductModelStoreOrModifiedRequest $request)
    {
        $item = ProductModel::store($request, $id);

        if(!$item)
            return fail('Product Model not found.');

        return ok();
    }

    public function delete($id)
    {
        $item = ProductModel::find($id);

        if(!$item)
            return fail('Product Model not found.');

        $item->delete();

        return ok();
    }
}
