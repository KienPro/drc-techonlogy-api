<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductTag\AdminProductTagStoreOrModifiedRequest;
use App\Models\ProductTag;

class ProductTagController extends Controller
{
    public function list()
    {
        $this->getListingParams();

        return ok(ProductTag::listAdmin($this->params));
    }

    public function listAll()
    {
        return ok(
            ProductTag::orderBy('name', 'asc')->get(['id', 'name'])->map(function($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->name,
                ];
            })
        );
    }

    public function create(AdminProductTagStoreOrModifiedRequest $request)
    {
        $ProductTag = ProductTag::store($request);

        return ok();
    }

    public function show($id)
    {
        $item = ProductTag::find($id);

        if(!$item)
            return fail('Product Tag not found.');

        return ok($item->only('id',  'name', 'sequence'));
    }

    public function update($id, AdminProductTagStoreOrModifiedRequest $request)
    {
        $item = ProductTag::store($request, $id);

        if(!$item)
            return fail('Product Tag not found.');

        return ok();
    }

    public function delete($id)
    {
        $item = ProductTag::find($id);

        if(!$item)
            return fail('Product Tag not found.');

        $item->delete();

        return ok();
    }
}
