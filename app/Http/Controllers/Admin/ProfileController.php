<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Profile\AdminUpdateProfileRequest;
use App\Http\Requests\Admin\Profile\AdminUpdatePasswordRequest;
use App\Models\User;
use DB;
use Exception;

class ProfileController extends Controller
{
    public function get()
    {
        try {
            auth('web')->user()->load('media');

            return ok(auth('web')->user());
        } catch (Exception $e) {
            report($e);
            return fail($e->getMessage(), 500);
        }
    }

    public function update(AdminUpdateProfileRequest $request)
    {
        try {
            return ok(User::updateProfile($request));
        } catch (Exception $e) {
            report($e);
            return fail($e->getMessage(), 500);
        }
    }

    public function updatePassword(AdminUpdatePasswordRequest $request)
    {
        try {
            DB::BeginTransaction();

            if(!User::updatePassword($request)) {
                return fail(__('messages.invalid.old_password'));
            }

            DB::commit();
            return ok();
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return fail($e->getMessage(), 500);
        }
    }
}
