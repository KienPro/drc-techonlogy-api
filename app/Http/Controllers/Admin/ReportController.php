<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Product;
use App\Models\Customer;
use App\Models\PaymentDetail;
use App\Http\Resources\Product\ProductListReportSaleAdminResource;
use App\Http\Resources\Customer\CustomerListReportSaleAdminResource;
use Carbon\Carbon;

class ReportController extends Controller
{
    public function sale()
    {
        $this->getListingParams();
        $this->addMoreParams(['customer_id', 'seller_id', 'from_date', 'to_date']);

     
        $res = Order::listReportAdmin($this->params);

        return ok($res);
    }

    public function payment()
    {
        $this->getListingParams();
        $this->addMoreParams(['customer_id','from_date', 'to_date', 'status', 'search']);

     
        $res = PaymentDetail::listReportAdmin($this->params);

        return ok($res);
    }

    public function customer()
    {
        $this->getListingParams();
        $this->addMoreParams(['from_date', 'to_date']);

        $res = Customer::listReportAdmin($this->params);

        return ok($res);
    }

    public function salesMonthlyDaily()
    {
        $years = [];
        $data = Order::select('grand_total', 'created_at')->get()
                                ->groupBy(function ($query){
                                    return Carbon::parse($query->created_at)->format('Y');
                                })
                                ->map(function ($items, $year) use (&$years){
                                    $years[] = $year;
                                    $months = [];

                                    $daily_orders = $items->groupBy(function ($query){
                                        return Carbon::parse($query->created_at)->format('m');
                                    })
                                    ->map(function ($items, $month) use (&$months){
                                        $months[] = $month;
                                        $daysInMonth = $items[0]->created_at->daysInMonth;
                                        $return = array_fill(0, $daysInMonth, 0);

                                        $items = $items->groupBy(function ($query){
                                            return Carbon::parse($query->created_at)->format('d');
                                        });

                                        foreach ($items as $key => $item) {
                                            $return[intval($key) - 1] = $item->sum('grand_total');
                                        }

                                        return [
                                            'data' => $return,
                                            'labels' => range(1, $daysInMonth)
                                        ];
                                    });

                                    return [
                                        'months' => $months,
                                        'daily_orders' => $daily_orders
                                    ];
                                });

        return ok([
            'years' => $years,
            'data' => $data
        ]);
    }
}
