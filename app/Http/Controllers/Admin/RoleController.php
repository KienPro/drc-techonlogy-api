<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Role\AdminRoleStoreRequest;
use App\Http\Requests\Role\AdminRoleUpdateRequest;
use App\Http\Requests\General\StatusUpdateRequest;
use App\Http\Resources\Role\RoleShowAdminResource;
use Exception;
use DB;
use App\Models\Role;

class RoleController extends Controller
{
    public function list()
    {
        try {
            $this->getListingParams();
            $this->params['enable_status'] = request('enable_status');

            $list = Role::listAdmin($this->params);

            return ok($list);
        } catch (Exception $e) {
            return fail($e->getMessage(), 500);
        }
    }

    public function listAll()
    {
        try {
            $list = Role::listAll();

            return ok($list);
        } catch (Exception $e) {
            return fail($e->getMessage(), 500);
        }
    }

    /**
     * Get Detail of Role
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            if($id == 1) return fail('Role not found.');

            $item = Role::with('permissions')->find($id);

            if(!$item) return fail('Role not found.');

            return ok(new RoleShowAdminResource($item));
        } catch (Exception $e) {
            return fail($e->getMessage(), 500);
        }
    }

    public function create(AdminRoleStoreRequest $request)
    {
        DB::beginTransaction();
        try {
            Role::createAdmin($request);

            DB::commit();
            return ok();
        } catch (Exception $e) {
            DB::rollback();

            return fail($e->getMessage(), 500);
        }
    }

    public function update(AdminRoleUpdateRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            if($id == 1) return fail('Role not found.');

            $item = Role::updateAdmin($request, $id);

            if(!$item) return fail('Role not found.');

            DB::Commit();
            return ok();
        } catch (Exception $e) {
            DB::rollback();

            return fail($e->getMessage(), 500);
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $item = Role::find($id);

            if (is_null($item))
                return fail('Role not found.');

            if(!$item->can_delete)
                return fail('Role is already in used. Can not deleted.');

            $item->delete();

            DB::commit();

            return ok();
        } catch (Exception $e) {
            DB::rollback();

            return fail($e->getMessage(), 500);
        }
    }

    public function updateStatus(StatusUpdateRequest $request, $id)
    {
        try {
            if($id == 1) return fail('', 404);

            $item = Role::find($id);

            if (!$item)
                return fail('Role not found.');

            $item->update($request->only('enable_status'));

            return ok($item);
        } catch (Exception $e) {
            return fail($e->getMessage(), 500);
        }
    }
}
