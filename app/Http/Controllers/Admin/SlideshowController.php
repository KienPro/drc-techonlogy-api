<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Slideshow\AdminSlideshowStoreOrModifiedRequest;
use App\Models\Slideshow;

class SlideshowController extends Controller
{
    public function list()
    {
        $this->getListingParams();
        $this->params['enable_status'] = request('enable_status');

        $data = Slideshow::listAdmin($this->params);

        return ok($data);
    }

    public function store(AdminSlideshowStoreOrModifiedRequest $request)
    {
        $Slideshow = Slideshow::store($request);

        return ok();
    }

    public function show($id)
    {
        $item = Slideshow::find($id);
        if(!$item)
            return fail('Slideshow not found.');

        $item->load('media');

        return ok($item->only(
            'id',
            'title',
            'product_name',
            'tag_line',
            'url',
            'button_name',
            'enable_status',
            'sequence',
            'media',
        ));
    }

    public function update($id, AdminSlideshowStoreOrModifiedRequest $request)
    {
        $item = Slideshow::updateSlideshow($request, $id);

        if(!$item)
            return fail('Slideshow not found.');

        return ok();
    }

    public function delete($id)
    {
        $item = Slideshow::find($id);

        if(!$item)
            return fail('Slideshow not found.');

        $item->delete();

        return ok();
    }
}
