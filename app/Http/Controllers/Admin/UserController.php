<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\General\StatusUpdateRequest;
use App\Http\Requests\User\AdminUserStoreRequest;
use App\Http\Requests\User\AdminUserUpdateRequest;
use App\Http\Resources\User\AdminUserResource;
use App\Models\User;
use DB;
use Exception;

class UserController extends Controller
{
    /**
     * Get List of User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        try {
            $this->getListingParams();
            $list = User::listAdmin($this->params);

            return ok($list);
        } catch (Exception $e) {
            report($e);
            return fail($e->getMessage(), 500);
        }
    }

    /**
     * Get List of User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function listAll()
    {
        try {
            return ok(
                User::active()->get()->map(function($item) {
                    return [
                        'id' => $item->id,
                        'name' => $item->username,
                    ];
                })
            );
        } catch (Exception $e) {
            report($e);
            return fail($e->getMessage(), 500);
        }
    }

    /**
     * User Detail
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            $user = User::find($id);

            if(!$user) {
                return fail('User not found.');
            }

            return ok(new AdminUserResource($user));
        } catch (Exception $e) {
            report($e);
            return fail($e->getMessage(), 500);
        }
    }

    /**
     * Create User
     *
     * @param AdminUserStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(AdminUserStoreRequest $request)
    {
        try {
            DB::beginTransaction();
            User::createUser($request);
            DB::commit();
            return ok();
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            return fail($e->getMessage(), 500);
        }
    }

    /**
     * Create User
     *
     * @param AdminUserUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(AdminUserUpdateRequest $request)
    {
        try {
            DB::beginTransaction();
            $user = User::updateUser($request);
            if(!$user) {
                return fail('User not found.');
            }
            DB::commit();
            return ok();
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            return fail($e->getMessage(), 500);
        }
    }

    /**
     * Update User Status
     *
     * @param StatusUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateStatus(StatusUpdateRequest $request)
    {
        try {
            DB::beginTransaction();
            User::updateUserStatus($request);
            DB::commit();
            return ok();
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            return fail($e->getMessage(), 500);
        }
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            User::where('id', $id)->delete();
            DB::commit();
            return ok();
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            return fail($e->getMessage(), 500);
        }
    }
}
