<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;
use App\Models\OrderDetail;
use App\Models\PaymentDetail;
use App\Http\Requests\Order\AdminOrderUpdateRequest;
use App\Http\Requests\Payment\AdminPaymentStoreRequest;
use Carbon\Carbon;

class ViewOrderController extends Controller
{
    public function list()
    {
        $this->getListingParams();
        $this->params['status'] = request('status');
        $this->params['from_date'] = request('from_date');
        $this->params['to_date'] = request('to_date');

        return ok(Order::listAdmin($this->params));
    }

    public function count()
    {
        return ok(Order::where('status', 'pending')->count());        
    }

    public function update($id, AdminOrderUpdateRequest $request)
    {
        $item = Order::change($request, $id);

        if(!$item)
            return fail('Order not found.');

        return ok();
    }

    public function invoice($code)
    {
        $order = Order::with('orderDetails')
                        ->where('code', $code)
                        ->first();
        if(!$order) {
            return fail('Order not found.');
        }
        $data = $order->only('id', 'code','total') + [
            'seller' => $order->user->username,
            'seller_phone' => $order->user->phone_number ?? Null,
            'order_date' => Carbon::parse($order->date)->format('d/M/Y'),
            'customer' => $order->customer->only('name', 'address', 'phone_number', 'email'),
            'products' => $order->orderDetails->map(function($product){
                return $product->only('product_name', 'qty', 'price', 'amount') + [
                    'product' => Product::where('id', $product->product_id)->with('media')->first()
                ];
            }),
        ];

        return $data;
    }

    public function payment($id, AdminPaymentStoreRequest $request)
    {
        $item = Order::find($id);

        if(!$item)
            return fail('Order not found.');

        PaymentDetail::store($request, $item->id);

        // if($item->grand_total <= $item->payments()->sum('payment_amount'))
        //     $item->update(['status' => 'completed']);

        return ok();
    }

    public function removeProduct($id)
    {
        $item = orderDetail::find($id);
        $order = $item->where('order_id', $item->order_id)->count();

        if($order == 1){
            $found_order = Order::where('id', $item->order_id)->first();
            $found_order->update(['status' => 'rejected']);
        }

        if(!$item)
            return fail('Product not found.');
        $item->delete();

        return ok();
    }
}

