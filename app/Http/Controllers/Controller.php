<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * Array parameters.
     *
     * @var string
     */
    protected $params = [];

    /*
    Response success function
        */
    public function ok($data = null)
    {
        return response()->json(['success' => true, 'data' => $data]);
    }

    /*
    Response error function
        */
    public function fail($message, $code = 400)
    {
        return response()->json(["success" => false, "message" => $message], $code);
    }

    public function getListingParams($order = 'created_at', $sort = 'desc', $limit = 10)
    {
        $this->params['offset'] = request('offset') ?? 0;
        $this->params['limit'] = request('limit') ?? $limit;
        $this->params['search'] = request('search');
        $this->params['order'] = request('order') ?? $order;
        $this->params['sort'] = request('sort') ?? $sort;
        $this->params['export'] = request('export') ?? false;
    }

    public function addMoreParams($more_params) {
        foreach ($more_params as $param) {
            $this->params[$param] = request($param);
        }
    }
}
