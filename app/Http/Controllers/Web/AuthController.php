<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Customer\Auth\CustomerLoginRequest;
use App\Http\Requests\Customer\Auth\CustomerRegisterRequest;
use App\Http\Requests\Customer\Auth\CustomerForgotPasswordResetRequest;
use App\Models\CustomerForgetToken;
use App\Models\Customer;
Use DB;


class AuthController extends Controller
{
    public function register(CustomerRegisterRequest $request)
    {
        $customer = Customer::register($request);
        return ok($customer);
    }

    public function login(CustomerLoginRequest $request)
    {
        $auth = Customer::login($request);

        if(!$auth) return fail('Invalid Email or Password.');
        return ok($auth);
    }

    public function forgotPassword(Request $request)
    {
        DB::beginTransaction();
        $customer = Customer::where('email', $request->email)->first();

        if (!$customer)
            return fail('Invalid email address.');

        CustomerForgetToken::generateForgetCode($customer);

        DB::commit();
        return ok();
    }

    public function forgotPasswordVerify(Request $request)
    {
        DB::beginTransaction();
        $token = CustomerForgetToken::verifyCode($request->email, $request->verify_code);

        if(!$token)
            return fail(__('messages.invalid.verify_code'));

        DB::commit();
        return ok(['token' => $token]);
    }

    public function forgotPasswordReset(CustomerForgotPasswordResetRequest $request)
    {
        DB::beginTransaction();
        if(!CustomerForgetToken::verifyForgetToken($request->token, $request->email)) {
            return fail(__('messages.can_not.reset_password'));
        }

        $user = Customer::where('email', $request->email)->first();

        if(!$user) {
            return fail(__('messages.can_not.reset_password'));
        }

        $user->update(['password' => bcrypt($request->password)]);

        DB::commit();
        return ok(__('messages.reset_password_successfully'));
    }

}
