<?php

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;
use App\Models\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function list()
    {
        $this->getListingParams();
        $data = Cart::listWeb($this->params);

        return ok($data);
    }
    
    public function singleCart(Request $request)
    {
        $exist_cart = Cart::where('product_id', $request->product_id)->first();
        // $exist_cart->update(["qty" => $request->qty]);
        // if($exist_cart){
        //     return fail('Product exist in exist_cart!');
        // }
        $cart = Cart::store($request);

        return ok();  
    }

    public function multipleCart(Request $request)
    {
        info($request->all());
        $exist_cart = Cart::where('product_id', $request->product_id)->first();
        // $exist_cart->update(["qty" => $request->qty]);
        // if($exist_cart){
        //     return fail('Product exist in exist_cart!');
        // }
        $cart = Cart::store($request);

        return ok();  
    }

    public function delete($id)
    {
        $item = Cart::find($id);
        if(!$item)
            return fail('Cart not found.');
        $item->delete();

        return ok();     
    }

    public function count()
    {
        return ok([
            'count' => Cart::where('customer_id', auth()->user()->id)->where('order_id', null)->count(),
            'total' => Cart::where('customer_id', auth()->user()->id)->where('order_id', null)->sum('price')
        ]);
    }
}