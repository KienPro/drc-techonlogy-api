<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Http\Requests\Order\CustomerOrderStoreRequest;

class OrderController extends Controller
{
    public function store(CustomerOrderStoreRequest $request)
    { 
        $order = Order::store($request);
        return ok(); 
    }
}
