<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProductCategory;
use App\Http\Resources\ProductCategory\ProductCategoryListWebResource;

class ProductCategoryController extends Controller
{
    public function list()
    {
        $this->getListingParams();
        // $this->params['restaurant_id'] = request('restaurant_id');
        return ok(ProductCategoryListWebResource::collection(ProductCategory::listWeb($this->params)));
    }
}
