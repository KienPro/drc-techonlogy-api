<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Resources\Product\ProductQuickViewWebResource;
use App\Http\Resources\Product\ProductDetailWebResource;
use App\Http\Resources\Product\ProductCompareWebResource;

class ProductController extends Controller
{    
    public function list()
    {
        $order = request('order') ?? 'name';
        $this->getListingParams($order, 'asc',12);
        $this->params['product_category_id'] = request('product_category_id');
        $this->params['product_model_id'] = request('product_model_id');
        $this->params['minval'] = request('minval') ?? 0;
        $this->params['maxval'] = request('maxval') ?? 3000;
        $this->params['product_tag_id'] = request('product_tag_id');
        $this->params['is_bestseller'] = request('is_bestseller');

        $data = Product::listWeb($this->params);
        
        $data['list']->each->append('string_tags');
        return ok($data);
    }

    public function quickview($id)
    {
        $item = Product::find($id);

        if(!$item || (!$item->enable_status))
            return fail('Product not found.');

        return ok(new ProductQuickViewWebResource($item));
    }

    public function details($id)
    {
        $item = Product::find($id);

        if(!$item || (!$item->enable_status))
            return fail('Product not found.');

        return ok(new ProductDetailWebResource($item));
    }

    public function compare($id)
    {
        $item = Product::find($id);

        if(!$item || (!$item->enable_status))
            return fail('Product not found.');

        return ok(new ProductCompareWebResource($item));
    }
}
