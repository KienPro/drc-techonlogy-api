<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProductModel;
use App\Http\Resources\ProductModel\ProductModelListWebResource;


class ProductModelController extends Controller
{
    public function list()
    {
        $this->getListingParams();
        // $this->params['restaurant_id'] = request('restaurant_id');
        return ok(ProductModelListWebResource::collection(ProductModel::listWeb($this->params)));
    }
}
