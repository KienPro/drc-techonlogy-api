<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProductTag;
use App\Http\Resources\ProductTag\ProductTagListWebResource;

class ProductTagsController extends Controller
{
    public function list()
    {
        try {
            $this->getListingParams('sequence', 'asc');

            return ok(ProductTagListWebResource::collection(ProductTag::listWeb($this->params)));
        } catch (Exception $e) {
            report($e);
            return fail($e->getMessage(), 500);
        }
    }
}
