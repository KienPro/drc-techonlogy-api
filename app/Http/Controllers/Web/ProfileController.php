<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Customer\Profile\CustomerUpdateProfileRequest;
use App\Http\Requests\Customer\Profile\CustomerUpdatePasswordRequest;
use App\Models\Customer;
use DB;
use Exception;

class ProfileController extends Controller
{
    public function get()
    {
        try {
            auth('customers')->user()->load('media');
            
            return ok(auth('customers')->user());
        } catch (Exception $e) {
            report($e);
            return fail($e->getMessage(), 500);
        }
    }

    public function update(CustomerUpdateProfileRequest $request)
    {
        try {
            return ok(Customer::updateProfile($request));
        } catch (Exception $e) {
            report($e);
            return fail($e->getMessage(), 500);
        }
    }

    public function updatePassword(CustomerUpdatePasswordRequest $request)
    {
        try {
            DB::BeginTransaction();

            if(!Customer::updatePassword($request)) {
                return fail(__('messages.invalid.old_password'));
            }

            DB::commit();
            return ok();
        } catch (Exception $e) {
            DB::rollback();
            report($e);
            return fail($e->getMessage(), 500);
        }
    }
}
