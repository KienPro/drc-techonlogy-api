<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Slideshow;

class SlideshowController extends Controller
{
    public function list()
    {
        $this->getListingParams();
        $this->params['enable_status'] = request('enable_status');
        $this->params['sequence'] = request('sequence');

        $data = Slideshow::listWeb($this->params);

        return ok($data);
    }

}
