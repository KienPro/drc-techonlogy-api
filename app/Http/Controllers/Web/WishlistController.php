<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Wishlist\CustomerWishlistStoreOrUpdateRequest;
use App\Models\Wishlist;
use App\Models\Product;


class WishlistController extends Controller
{
    public function list()
    {
        $this->getListingParams();
        $this->params['enable_status'] = request('product_id');
        $data = Wishlist::listWeb($this->params);
        
        return ok($data);
    }

    public function wish(CustomerWishlistStoreOrUpdateRequest $request)
    {
        $exist_wishlist = Wishlist::where('product_id', $request->product_id)->where('cart_id', null)->first();
        if($exist_wishlist){
            return fail('Product already exist in wishlist!');
        }
        $wishlist = Wishlist::store($request);

        return ok();        
    }

    public function unwish($id, CustomerWishlistStoreOrUpdateRequest $request)
    {
        $item = Wishlist::where('product_id', $id)->first();
        if(!$item)
            return fail('Product not found.');
        $item->delete();

        return ok();     
    }

    public function count()
    {
        return ok(Wishlist::where('customer_id', auth()->user()->id)
                            ->where('cart_id', null)
                            ->count());        
    }
}
