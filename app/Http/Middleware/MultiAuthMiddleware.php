<?php

namespace App\Http\Middleware;

use Closure;

class MultiAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        switch($guard)
        {
            case 'web':
                if(class_basename(auth()->user()) != 'User') return fail('Unauthenticated.', 401);
                break;
            case 'customers':
                if(class_basename(auth()->user()) != 'Customer') return fail('Unauthenticated.', 401);
                break;
            default:
                return fail('Unauthenticated.', 401);
        }

        auth($guard)->login(auth()->user());

        return $next($request);
    }
}
