<?php

namespace App\Http\Requests\Admin\Profile;

use App\Http\Requests\DefaultFormRequest;

class AdminUpdateProfileRequest extends DefaultFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|max:255|unique:users,email,' . auth('web')->user()->id . ',id,deleted_at,NULL',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'username' => 'required|max:255|unique:users,username,' . auth('web')->user()->id . ',id,deleted_at,NULL',
            'phone_number' => 'required'
        ];
    }
}
