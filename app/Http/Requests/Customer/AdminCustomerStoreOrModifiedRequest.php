<?php

namespace App\Http\Requests\Customer;

use App\Http\Requests\DefaultFormRequest;

class AdminCustomerStoreOrModifiedRequest extends DefaultFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'phone_number' => 'required',
        ];
    }
}
