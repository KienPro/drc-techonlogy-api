<?php

namespace App\Http\Requests\Customer\Auth;

use App\Http\Requests\DefaultFormRequest;

class CustomerForgotPasswordResetRequest extends DefaultFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|max:255',
            'password' => 'required|min:8',
            'token' => 'required'
        ];
    }
}
