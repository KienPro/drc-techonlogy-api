<?php

namespace App\Http\Requests\Customer\Auth;

use App\Http\Requests\DefaultFormRequest;

class CustomerRegisterRequest extends DefaultFormRequest
{
    public function rules()
    {
        return [
            'name' => 'required',
            'phone_number' => 'required|phone:kh',
            'email' => 'required|email|max:255|unique:customers,email,NULL,id,deleted_at,NULL',
            'password' => 'required|min:8',
        ];
    }
}
