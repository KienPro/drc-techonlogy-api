<?php

namespace App\Http\Requests\Customer\Profile;


use App\Http\Requests\DefaultFormRequest;

class CustomerUpdatePasswordRequest extends DefaultFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required',
            'password' => 'required',
        ];
    }
}
