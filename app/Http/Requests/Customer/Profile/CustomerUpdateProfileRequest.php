<?php

namespace App\Http\Requests\Customer\Profile;

use App\Http\Requests\DefaultFormRequest;

class CustomerUpdateProfileRequest extends DefaultFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required'
        ];
    }
}
