<?php

namespace App\Http\Requests\Order;

use App\Http\Requests\DefaultFormRequest;

class AdminOrderUpdateRequest extends DefaultFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'status' => 'required',
        ];
    }
}
