<?php

namespace App\Http\Requests\Order;

use App\Http\Requests\DefaultFormRequest;

class CustomerOrderStoreRequest extends DefaultFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'customer_id' => 'required',
        ];
    }
}
