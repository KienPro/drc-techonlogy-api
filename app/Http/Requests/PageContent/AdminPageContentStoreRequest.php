<?php

namespace App\Http\Requests\PageContent;

use App\Http\Requests\DefaultFormRequest;

class AdminPageContentStoreRequest extends DefaultFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $return = [
            'page_id' => 'required'
        ];

        switch($this->content_type) {
            case 'text':
                $return += [
                    'title_en' => 'required',
                    'title_kh' => 'required',
                    'text_en' => 'required',
                    'text_kh' => 'required',
                ];
                break;
            case 'image':
                $return += [
                    'image' => 'required'
                ];
                break;
            case 'video':
                $return += [
                    'video_url' => 'required'
                ];
                break;
            default:
                $return += [
                    'content_type' => 'required|in:text'
                ];
        }

        return $return;
    }
}
