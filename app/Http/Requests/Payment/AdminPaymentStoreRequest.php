<?php

namespace App\Http\Requests\Payment;

use App\Http\Requests\DefaultFormRequest;

class AdminPaymentStoreRequest extends DefaultFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required'
        ];
    }
}
