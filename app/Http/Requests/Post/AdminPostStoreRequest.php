<?php

namespace App\Http\Requests\Post;

use App\Http\Requests\DefaultFormRequest;

class AdminPostStoreRequest extends DefaultFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "text" => "required",
            "restaurant_id" => "required",
            "image" => "required"
        ];
    }
}
