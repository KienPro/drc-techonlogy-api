<?php

namespace App\Http\Requests\PostContent;

use App\Http\Requests\DefaultFormRequest;

class AdminPostContentUpdateRequest extends DefaultFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $return = [];

        switch($this->content_type) {
            case 'text':
                $return += [
                    'text' => 'required',
                ];
                break;
            case 'image':
                $return += [
                    'image' => 'required'
                ];
                break;
            case 'video':
                $return += [
                    'video_url' => 'required'
                ];
                break;
            default:
                $return += [
                    'content_type' => 'required|in:text'
                ];
        }

        return $return;
    }
}
