<?php

namespace App\Http\Requests\Product;

use App\Http\Requests\DefaultFormRequest;

class AdminProductStoreOrModifiedRequest extends DefaultFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'product_category_id' => 'required',
            'product_model_id' => 'required',
            'name' => 'required',
            'is_new' => 'required',
            'old_price' => 'required|numeric',
            'current_price' => 'required|numeric',
        ];

        if(!request()->route('id'))
            $rules['image'] = 'required';

        return $rules;
    }
}
