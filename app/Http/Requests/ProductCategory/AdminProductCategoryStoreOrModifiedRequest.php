<?php

namespace App\Http\Requests\ProductCategory;

use App\Http\Requests\DefaultFormRequest;

class AdminProductCategoryStoreOrModifiedRequest extends DefaultFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
        ];
    }
}
