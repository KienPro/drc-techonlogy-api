<?php

namespace App\Http\Requests\ProductImage;

use App\Http\Requests\DefaultFormRequest;

class AdminProductImageModifiedRequest extends DefaultFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}
