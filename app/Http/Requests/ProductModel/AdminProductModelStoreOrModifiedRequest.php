<?php

namespace App\Http\Requests\ProductModel;

use App\Http\Requests\DefaultFormRequest;

class AdminProductModelStoreOrModifiedRequest extends DefaultFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_category_id' => 'required',
            'name' => 'required',
        ];
    }
}
