<?php

namespace App\Http\Requests\ProductTag;

use App\Http\Requests\DefaultFormRequest;

class AdminProductTagStoreOrModifiedRequest extends DefaultFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
        ];
    }
}
