<?php

namespace App\Http\Requests\Role;

use App\Http\Requests\DefaultFormRequest;

class AdminRoleStoreRequest extends DefaultFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:roles,name,NULL,id,deleted_at,NULL',
        ];
    }
}
