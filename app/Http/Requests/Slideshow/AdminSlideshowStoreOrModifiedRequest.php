<?php

namespace App\Http\Requests\SlideShow;

use App\Http\Requests\DefaultFormRequest;

class AdminSlideshowStoreOrModifiedRequest extends DefaultFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        if(!request()->route('id'))
        $rules['image'] = 'required';

        return $rules;
    }
}
