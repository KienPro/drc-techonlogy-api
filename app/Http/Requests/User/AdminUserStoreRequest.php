<?php

namespace App\Http\Requests\User;

use App\Http\Requests\DefaultFormRequest;

class AdminUserStoreRequest extends DefaultFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|unique:users,username,NULL,id,deleted_at,NULL',
            'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
            'gender' => 'required',
            'password' => 'required|min:8',
            'phone_number' => 'required',
            'enable_status' => 'required'
        ];
    }
}
