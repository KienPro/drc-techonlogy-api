<?php

namespace App\Http\Requests\Wishlist;

use App\Http\Requests\DefaultFormRequest;

class CustomerWishlistStoreOrUpdateRequest extends DefaultFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'customer_id' => 'required',
        ];
    }
}
