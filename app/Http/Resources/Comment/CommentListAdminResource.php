<?php

namespace App\Http\Resources\Comment;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentListAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->only('id', 'comment', 'amount_likes', 'created_at', 'hide_at') + [
            'customer' => $this->customer->only('id', 'name', 'phone_number', 'media')
        ];
    }
}
