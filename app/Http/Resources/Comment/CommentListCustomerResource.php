<?php

namespace App\Http\Resources\Comment;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentListCustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->only('id', 'comment', 'amount_likes') + [
            'comment_at' => $this->created_at->timestamp,
            'is_liked' => $this->is_customer_liked,
            'customer' => $this->customer->only('id', 'name', 'phone_number') + ['url' => $this->media->url ?? null]
        ];
    }
}
