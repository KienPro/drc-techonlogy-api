<?php

namespace App\Http\Resources\Customer;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerListReportSaleAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $total = 0;

        return $this->only('id', 'name', 'phone_number', 'platform') + [
            'group' => $this->customer_group->name ?? null,
            'paid' => $this->orders->sum(function($order) use (&$total){
                $total += $order->details->sum('total');
                return $order->payments->sum('payment_amount');
            }),
            'total' => $total
        ];
    }
}
