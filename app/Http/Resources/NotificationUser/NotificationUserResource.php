<?php

namespace App\Http\Resources\NotificationUser;

use App\Helper\DateHelper;
use Illuminate\Http\Resources\Json\JsonResource;

class NotificationUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $image = null;
        if(in_array($this->notification->type, ['post', 'product'])) {
            $image = $this->notification->from->restaurant->media->url ?? null;
        } else if($this->notification->type == 'restaurant'){
            $image = $this->notification->from->media->url ?? null;
        }

        return [
            'id' => $this->id,
            'read' => !!$this->read,
            'title' => app()->getLocale() == 'kh' ? $this->notification->title_kh : $this->notification->title_en,
            'message' => app()->getLocale() == 'kh' ? $this->notification->message_kh : $this->notification->message_en,
            'type' => $this->notification->type,
            'type_id' => $this->notification->type_id,
            'created_at' => DateHelper::getMobileDateTimeString($this->notification->created_at),
            'image' => $image
        ];
    }
}
