<?php

namespace App\Http\Resources\Page;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\PageContent\PageContentListResource;

class PageShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->only('id') + [
            'title' => app()->getLocale() == 'kh' ? $this->title_kh : $this->title_en,
            'short_description' => app()->getLocale() == 'kh' ? $this->short_description_kh : $this->short_description_en,
            'media' => $this->media,
            'contents' => PageContentListResource::collection($this->contents->sortBy('sequence'))
        ];
    }
}
