<?php

namespace App\Http\Resources\PageContent;

use Illuminate\Http\Resources\Json\JsonResource;

class PageContentAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->only('id', 'content_type', 'title_en', 'title_kh', 'text_en', 'text_kh', 'video_url', 'sequence') + [
            'media' => $this->media
        ];
    }
}
