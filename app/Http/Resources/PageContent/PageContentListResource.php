<?php

namespace App\Http\Resources\PageContent;

use Illuminate\Http\Resources\Json\JsonResource;

class PageContentListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        switch ($this->content_type) {
            case 'text':
                $data = [
                    'title' => app()->getLocale() == 'kh' ? $this->title_kh : $this->title_en,
                    'text' => app()->getLocale() == 'kh' ? $this->text_kh : $this->text_en,
                ];
                break;
            case 'image':
                $data = [
                    'media' => $this->media
                ];
                break;
            case 'video':
                $data = [
                    'video_url' => $this->video_url
                ];
                break;
            default:
                $data = [];
        }

        return $this->only('id', 'content_type') + $data;
    }
}
