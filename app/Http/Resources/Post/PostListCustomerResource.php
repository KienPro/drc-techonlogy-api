<?php

namespace App\Http\Resources\Post;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Helper\DateHelper;

class PostListCustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->only('id', 'text', 'amount_views', 'amount_likes', 'amount_comments', 'amount_share', 'share_url') + [
            'image' => $this->media->url ?? null,
            'publish_at' => DateHelper::getMobileDateTimeString($this->publish_at),
            'restaurant' => $this->restaurant->only('id') + [
                'name' => app()->getLocale() == 'kh' ? $this->restaurant->name_kh : $this->restaurant->name_en,
                'image' => $this->restaurant->media->url
            ],
            'is_liked' => $this->is_customer_liked
        ];
    }
}
