<?php

namespace App\Http\Resources\Post;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\PostContent\PostContentListCustomerResource;

class PostPreviewAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->only('id', 'text', 'amount_views', 'amount_likes', 'amount_comments', 'amount_share', 'publish_at') + [
            'image' => $this->media->url ?? null,
            'restaurant' => $this->restaurant->only('id') + [
                'name' => $this->restaurant->name_en,
                'profile' => $this->restaurant->media->url ?? null
            ],
            'contents' => PostContentListCustomerResource::collection($this->contents->sortBy('sequence'))
        ];
    }
}
