<?php

namespace App\Http\Resources\PostContent;

use Illuminate\Http\Resources\Json\JsonResource;

class PostContentAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->only('id', 'content_type', 'text', 'video_url', 'sequence') + [
            'media' => $this->media
        ];
    }
}
