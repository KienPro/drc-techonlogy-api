<?php

namespace App\Http\Resources\PostContent;

use Illuminate\Http\Resources\Json\JsonResource;

class PostContentListCustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        switch ($this->content_type) {
            case 'text':
                $data = [
                    'text' => $this->text,
                ];
                break;
            case 'image':
                $data = [
                    'image' => $this->media->url ?? null
                ];
                break;
            case 'video':
                $data = [
                    'video_url' => $this->video_url
                ];
                break;
            default:
                $data = [];
        }

        return $this->only('id', 'content_type') + $data;
    }
}
