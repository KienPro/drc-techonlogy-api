<?php

namespace App\Http\Resources\Product;

use App\Helper\DateHelper;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductCompareWebResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->only('id', 'name', 'current_price', 'specification', ) + [
                'image' => $this->media->url,
                'product_category' => $this->product_category->name,
                'product_model' => $this->product_model->name,
            ];
    }
}
