<?php

namespace App\Http\Resources\Product;

use App\Helper\DateHelper;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductListCustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->only('id', 'price', 'short_description', 'amount_likes', 'amount_comments', 'amount_share', "share_url") + [
            'name' => app()->getLocale() == 'kh' ? $this->name_kh : $this->name_en,
            'category' => app()->getLocale() == 'kh' ? optional($this->product_category)->name_kh : optional($this->product_category)->name_en,
            'image' => $this->media->url ?? null,
            'is_liked' => $this->is_customer_liked
        ];
    }
}
