<?php

namespace App\Http\Resources\Product;

use App\Helper\DateHelper;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductQuickViewWebResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->only(
            'id', 'name', 'old_price', 'current_price', 'short_description') + [
                'images' => collect([$this->media->url])->merge($this->images->pluck('media.url')->toArray())
        ];
    }
}
