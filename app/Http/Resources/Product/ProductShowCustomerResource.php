<?php

namespace App\Http\Resources\Product;

use App\Helper\DateHelper;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ProductVariant\ProductVariantListCustomerResource;

class ProductShowCustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->only('id', 'current_price', 'short_description', 'long_description', 'amount_likes', 'amount_comments', 'amount_share', "share_url") + [
            'name' => app()->getLocale() == 'kh' ? $this->name_kh : $this->name_en,
            'category' => app()->getLocale() == 'kh' ? optional($this->product_category)->name_kh : optional($this->product_category)->name_en,
            'is_liked' => $this->is_customer_liked,
            'variants' => ProductVariantListCustomerResource::collection($this->variants),
            'restaurant' => [
                'id' => $this->restaurant->id,
                'name' => app()->getLocale() == 'kh' ? $this->restaurant->name_kh : $this->restaurant->name_en,
                'image' => $this->restaurant->media->url
            ],
            'images' => $this->images->pluck('media.url')
        ];
    }
}
