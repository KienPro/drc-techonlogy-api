<?php

namespace App\Http\Resources\ProductCategory;

use App\Helper\DateHelper;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductCategoryListWebResource extends JsonResource
{
    public function toArray($request)
    {
        return $this->only('id', 'name', 'product_models');
    }
}
