<?php

namespace App\Http\Resources\ProductModel;

use App\Helper\DateHelper;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductModelListWebResource extends JsonResource
{
    public function toArray($request)
    {
        return $this->only('id', 'name', 'product_category_id');
    }
}
