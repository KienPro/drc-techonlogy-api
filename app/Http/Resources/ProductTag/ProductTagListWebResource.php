<?php

namespace App\Http\Resources\ProductTag;

use App\Helper\DateHelper;
use Illuminate\Http\Resources\Json\JsonResource;

class productTagListWebResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->only('id', 'name');
    }
}
