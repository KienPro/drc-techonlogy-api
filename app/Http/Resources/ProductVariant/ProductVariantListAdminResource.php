<?php

namespace App\Http\Resources\ProductVariant;

use App\Helper\DateHelper;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductVariantListAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->only('id') + [
            'name' => app()->getLocale() == 'kh' ? $this->name_kh : $this->name_en,
            'values' => app()->getLocale() == 'kh' ? $this->values->pluck('name_kh') : $this->values->pluck('name_en')
        ];
    }
}
