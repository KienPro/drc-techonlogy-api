<?php

namespace App\Http\Resources\Restaurant;

use App\Helper\DateHelper;
use Illuminate\Http\Resources\Json\JsonResource;

class RestaurantListMapCustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->only('id', 'latitude', 'longitude');
    }
}
