<?php

namespace App\Http\Resources\Restaurant;

use App\Helper\DateHelper;
use Illuminate\Http\Resources\Json\JsonResource;

class RestaurantShowMapCustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->only('id') + [
            'name' => app()->getLocale() == 'kh' ? $this->name_kh : $this->name_en,
            'category' => $this->restaurant_category ? (app()->getLocale() == 'kh' ? $this->restaurant_category->name_kh : $this->restaurant_category->name_en) : null,
            'logo' => $this->media->url ?? null,
            'banner' => $this->banner->url ?? null,
            'distance' => round($this->distance, 1) ?? null,
            'estimate_time' => $this->distance ? round($this->distance) * config('system.estimate_min_per_kilo') : null
        ];
    }
}
