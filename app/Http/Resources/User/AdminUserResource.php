<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class AdminUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->only('id', 'username', 'first_name', 'last_name', 'address', 'description', 'email', 'enable_status', 'gender', 'phone_number') +
        [
            'media' => $this->media,
            'roles' => $this->roles()->pluck('id')
        ];
    }
}
