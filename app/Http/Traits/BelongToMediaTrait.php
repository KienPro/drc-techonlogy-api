<?php

namespace App\Http\Traits;

use App\Models\Media;

trait BelongToMediaTrait
{
    public function media()
    {
        return $this->belongsTo(Media::class);
    }
}
