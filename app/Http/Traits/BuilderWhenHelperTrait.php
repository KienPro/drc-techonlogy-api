<?php

namespace App\Http\Traits;

trait BuilderWhenHelperTrait
{
    public function scopeWhenWhere($q, $key, $value)
    {
        $q->when(filled($value), function ($q) use ($key, $value) {
            $q->where($key, $value);
        });
    }

    public function scopeWhenWhereDate($q, $key, $comparison, $value)
    {
        $q->when($value, function ($q) use ($key, $comparison, $value) {
            $q->whereDate($key, $comparison, $value);
        });
    }

    public function scopeWhenWhereIn($q, $when, $key, $values)
    {
        $q->when($when, function ($q) use ($key, $values) {
            $q->whereIn($key, $values ?? []);
        });
    }

    public function scopeWhenWhereNullOrNot($q, $key, $value)
    {
        $q->when(isset($value), function ($q) use ($key, $value) {
            if($value)
                $q->where($key, '<>', null);
            else
                $q->whereNull($key);
        });
    }
}
