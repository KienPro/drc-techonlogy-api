<?php

namespace App\Http\Traits;

trait ListingTrait
{
    public function scopeOrderById($q, $order)
    {
        if($order != 'id' && $order != $this->getTable() .'.id') {
            return $q->orderBy($this->getTable() .'.id', 'desc');
        }
    }

    public function scopeSortList($query, $params)
    {
        if(isset($params['order_raw']))
            return $query->orderByRaw($params['order'], $params['sort'])->orderById($params['order']);

        return $query->orderBy($params['order'], $params['sort'])->orderById($params['order']);
    }

    public function scopeOffsetLimit($query, $params)
    {
        return $query
            ->offset($params['offset'])
            ->limit($params['limit'])
            ->get();
    }

    public function scopeSortLimitTotal($query, $params)
    {
        $total = $query->count();

        if(!isset($params['export']) || !$params['export']) {
            $list = $query->sortList($params)->offsetLimit($params);
        } else {
            $list = $query->sortList($params)->get();
        }

        return compact('list', 'total');
    }

    public function scopeLimitTotal($query, $params)
    {
        $total = $query->count();

        if(!isset($params['export']) || !$params['export']) {
            $list = $query->offsetLimit($params);
        } else {
            $list = $query->get();
        }

        return compact('list', 'total');
    }

    public function scopeSortLimit($query, $params)
    {
        if(isset($params['export']) && $params['export']) {
            return $query->sortList($params)->get();
        }

        return $query->sortList($params)->offsetLimit($params);
    }
}
