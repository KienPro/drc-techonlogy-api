<?php

namespace App\Http\Traits;

trait ModelHelperTrait
{
    public static function scopeActive($query)
    {
        return $query->where('enable_status', 1);
    }

    // if has id and dose not has item return false
    public static function updateOrFailOrCreate($data, $id = null)
    {
        if($id) {
            $item = self::find($id);

            if(!$item) return false;

            $item->update($data);
        } else
            $item = self::create($data);

        return $item;
    }
}
