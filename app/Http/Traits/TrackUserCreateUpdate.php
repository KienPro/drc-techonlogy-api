<?php

namespace App\Http\Traits;

use App\Models\User;

trait TrackUserCreateUpdate
{
    public static function bootTrackUserCreateUpdate()
    {
        // create a event to happen on updating
        static::updating(function ($table) {
            $table->updated_by = auth('web')->user()->id ?? null;
        });

        // create a event to happen on saving
        static::creating(function ($table) {
            $table->created_by = auth('web')->user()->id ?? null;
        });
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updater()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
}
