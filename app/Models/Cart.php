<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\BuilderWhenHelperTrait;
use App\Http\Traits\ListingTrait;
use App\Http\Traits\BelongToMediaTrait;
use App\Http\Traits\ModelHelperTrait;
class Cart extends Model
{
    use ListingTrait,
        BuilderWhenHelperTrait,
        BelongToMediaTrait,
        ModelHelperTrait;

    protected $fillable = [
        'product_id',
        'customer_id',
        'order_id',
        'qty',
        'amount',
        'price'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
    
    public function product(){
        return $this->belongsTo(Product::class);
    }

    public static function listweb($params)
    {
        return Cart::with('product.media')
                    ->where("customer_id", auth()->user()->id)
                    ->orderby('id', 'desc')
                    ->where('order_id', null)
                    ->sortLimitTotal($params);
                    
    }

    public static function store($request)
    {
        $data = $request->only('price', 'product_id', 'qty') + [
            'customer_id' => auth('customers')->user()->id,
            'amount' => $request->qty * $request->price
        ];
        $cart =  self::create($data);
        $wishlist = Wishlist::where('customer_id', auth('customers')->user()->id)
                            ->where('product_id', $request->product_id)
                            ->where('cart_id',null)->first();
        if($wishlist){
            $wishlist->update([
                'cart_id' => $cart->id
            ]);
        }
        return $cart;
    }
}
