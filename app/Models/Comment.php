<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Traits\BuilderWhenHelperTrait;
use App\Http\Traits\ListingTrait;
use App\Http\Traits\ModelHelperTrait;

class Comment extends Model
{
    use SoftDeletes,
        BuilderWhenHelperTrait,
        ListingTrait,
        ModelHelperTrait;

    protected $fillable = [
        'comment',
        'from_id',
        'from_type',
        'customer_id',
        'hide_at',
        'amount_likes'
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function likes()
    {
        return $this->morphMany(Like::class, 'from');
    }

    public function from()
    {
        return $this->morphTo();
    }

    public function getIsCustomerLikedAttribute()
    {
        if(!auth('customers')->user())
            return false;

        return !!$this->likes()->where('customer_id', auth('customers')->user()->id)->where('status', 'liked')->count();
    }

    public function scopeWhenSearch($q, $search)
    {
        $q->when($search, function ($q, $search) {
            $q->where('comment', 'LIKE', '%' . $search . '%')
                ->orWhereHas('customer', function($q) use ($search){
                    $q->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('phone_number', 'LIKE', '%' . $search . '%');
                });
        });
    }

    public function scopeWhereClassFromType($q, $type)
    {
        switch($type) {
            case 'product':
                $class = Product::class;
                break;
            case 'post':
                $class = Post::class;
                break;
            default:
                $class = null;
        }

        $q->where('from_type', $class);
    }

    public static function listAdmin($params)
    {
        $list = self::with('customer.media')
                    ->whenSearch($params['search'])
                    ->whereClassFromType($params['from_type'])
                    ->where('from_id', $params['from_id'])
                    ->whenWhereNullOrNot('hide_at', $params['is_hide'])
                    ->sortLimit($params);

        $totals = self::selectRaw('count(*) as total, SUM(case when hide_at is null then 0 else 1 end) as total_hide')
                        ->whenSearch($params['search'])
                        ->whereClassFromType($params['from_type'])
                        ->where('from_id', $params['from_id'])
                        ->whenWhereNullOrNot('hide_at', $params['is_hide'])
                        ->first();

        return [
            'list' => $list,
            'total' => $totals->total,
            'total_hide' => $totals->total_hide
        ];
    }

    public static function listPreviewAdmin($params)
    {
        return self::with('customer.media')
                    ->whereClassFromType($params['from_type'])
                    ->where('from_id', $params['from_id'])
                    ->sortLimitTotal($params);
    }

    public static function listCustomer($params)
    {
        return self::with('customer.media')
                    ->whereClassFromType($params['from_type'])
                    ->where('from_id', $params['from_id'])
                    ->whereNull('hide_at')
                    ->sortLimit($params);
    }

    public static function storeCustomer($from_id, $from_type, $comment)
    {
        $item = self::create([
            'from_id' => $from_id,
            'from_type' => $from_type,
            'customer_id' => auth('customers')->user()->id,
            'comment' => $comment
        ]);

        $item->from->increment('amount_comments');
    }
}
