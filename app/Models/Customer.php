<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authentication;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helper\MediaHelper;
use App\Http\Traits\ModelHelperTrait;
use App\Http\Traits\BelongToMediaTrait;
use App\Http\Traits\ListingTrait;
use App\Http\Traits\BuilderWhenHelperTrait;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Hash;

class Customer extends Authentication
{
    use SoftDeletes,
        ModelHelperTrait,
        BelongToMediaTrait,
        HasApiTokens,
        ListingTrait,
        BuilderWhenHelperTrait;

    protected $fillable = [
        'id',
        'name',
        'email',
        'password',
        // 'media_id',
        'phone_number',
        'address',
        'birthdate',
        'gender',
    ];

    protected $hidden = [
        'password'
    ];

    public function wishlist(){
        return $this->hasMany(Wishlist::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function scopeWhenSearch($q, $search)
    {
        $q->when(filled($search), function ($q) use ($search) {
            $q->where('name', 'LIKE', '%' . $search . '%')
                ->orWhere('email', 'LIKE', '%' . $search . '%')
                ->orWhere('phone_number', 'LIKE', '%' . $search . '%')
                ->orWhereHas('roles', function ($q) use ($search) {
                    $q->where('name', $search);
                });
        });
    }

    public static function listReportAdmin($params)
    {
        return self:: whenWhereDate('created_at', '>=', $params['from_date'])
                    ->whenWhereDate('created_at', '<=', $params['to_date'])
                    // ->filterListReport($params)
                    ->sortLimitTotal($params);
        
    }


    public static function getToken($customer = null)
    {        
        $customer = $customer ?? auth('customers')->user();
        return [
            'access_token' => $customer->createToken(auth('customers')->user()->email)->plainTextToken,
            'user' => $customer->load('media')
        ];
    }

    public static function register($request)
    {
        $customer = self::create([
            'name' => $request->name,
            'phone_number' => $request->phone_number,
            'address' => $request->address,
            'email' => $request->email,
            'birthdate' => $request->birthdate,
            'password' => Bcrypt($request->password),
        ]);
        auth('customers')->login($customer);

        return self::getToken($customer);
    }

    public static function login($request)
    {
        if (auth('customers')->attempt(['email' => $request['email'],
            'password' => $request['password'],
            'deleted_at' => null 
        ])) {
            return self::getToken();
        }
        return false;
    } 

    public static function updateProfile($request)
    {
        $data = $request->only('name', 'email', 'phone_number', 'gender', 'birthdate', 'address', 'description');
        $customer = auth('customers')->user()->update($data);
        return $customer;
    }

    public static function updatePassword($request)
    {
        $user = auth('customers')->user();

        if(Hash::check($request->old_password, $user->password)) {
            return $user->update([
                'password' => bcrypt($request->password)
            ]);
        }

        return false;
    }
}