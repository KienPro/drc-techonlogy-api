<?php

namespace App\Models;

use App\Helper\DateHelper;
use App\Helper\MailHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

class CustomerForgetToken extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'email',
        'verify_code',
        'expire_at',
        'is_verified',
        'reset_token'
    ];


    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public static function generateForgetCode($customer)
    {
        $code = Str::random(4);
        $now = DateHelper::getNow();
        $expire_minute = 15;

        self::create([
            'email' => $customer->email,
            'verify_code' => $code,
            'expire_at' => $now->addMinutes($expire_minute)
        ]);

        // send verification code here
        $data = [
            'code' => $code,
            'name'  => $customer->name
        ];

        $customer = [
            [
                'email' => $customer->email,
                'name'  => $customer->name
            ]
        ];

        MailHelper::sendMailUsers('mail.verifycode', 'Verify Code', $data, $customer);
    }

    public static function verifyCode($email, $code)
    {
        $now = DateHelper::getNow();

        $verify = self::where('email', $email)
                        ->where('verify_code', $code)
                        ->where('expire_at', '>', $now->toDateTimeString())
                        ->where('is_verified', 0)
                        ->get()
                        ->first();

        if($verify) {
            $verify->update([
                'reset_token' => Password::getRepository()->createNewToken(),
                'is_verified' => 1
            ]);

            return $verify->reset_token;
        }

        return false;
    }

    public static function verifyForgetToken($token, $email)
    {
        $verify = self::where('email', $email)
                        ->where('reset_token', $token)
                        ->get()
                        ->first();

        if($verify) {
            $verify->delete();

            return true;
        }

        return false;
    }
}
