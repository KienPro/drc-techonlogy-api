<?php

namespace App\Models;

use App\Helper\DateHelper;
use Illuminate\Database\Eloquent\Model;

class CustomerLoginAccess extends Model
{
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'authorized_token', 'access_token', 'expired_date', 'customer_id', 'created_at', 'updated_at', 'log_date'
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
