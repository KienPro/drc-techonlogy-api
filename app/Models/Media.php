<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;
use Aws\Exception\AwsException;

class Media extends Model
{
    protected $fillable = [
        'url', 'name'
    ];

    public function getUrlAttribute($value)
    {
        if(config('media.is_aws_s3')) {
            if(substr($value, 0, 1) == '/') $value = substr($value, 1);  

            return Storage::disk('s3')->url($value);
        }
        else
            return url('') . $value;
    }
}
