<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'sort', 'category', 'slug'
    ];

    public function actions()
    {
        return $this->belongsToMany(Action::class, 'permissions');
    }
}
