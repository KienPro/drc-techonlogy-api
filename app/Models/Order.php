<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\BuilderWhenHelperTrait;
use App\Http\Traits\ListingTrait;
use App\Http\Traits\ModelHelperTrait;
use App\Http\Traits\BelongToMediaTrait;
use App\Http\Traits\TrackUserCreateUpdate;

use Carbon\Carbon;

class Order extends Model
{
    use BuilderWhenHelperTrait,
        ListingTrait,
        TrackUserCreateUpdate,
        BelongToMediaTrait,
        ModelHelperTrait;

    protected $fillable = [
        'customer_id',
        'user_id',
        'code',
        'date',
        'total',
        'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function payments()
    {
        return $this->hasMany(PaymentDetail::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function scopeWhenSearch($q, $search)
    {
        $q->when($search, function($q, $search) {
            $q->where('code', 'LIKE', '%' . $search . '%');
        });
    }

    public static function listAdmin($params)
    {
        return self::with('customer', 'user')
                    ->with('orderDetails', 'payments.media', 'payments.creator')
                    ->whenWhereDate('date', '>=', $params['from_date'])
                    ->whenWhereDate('date', '<=', $params['to_date'])

                    ->whenWhere('status', $params['status'])
                    ->whenSearch($params['search'])
                    ->sortLimitTotal($params);
    }
    

    public static function listReportAdmin($params)
    {
        return self::with('customer', 'orderDetails', 'user')
                    
                    ->whenWhereDate('created_at', '>=', $params['from_date'])
                    ->whenWhereDate('created_at', '<=', $params['to_date'])
                    // ->filterListReport($params)
                    ->sortLimitTotal($params);
        
    }

    public static function store($request)
    {
        $request_order = [
            'customer_id' => auth('customers')->user()->id,
            'user_id' => null,
            'code' => OrderNumber::getOrderNumber(),
            'date' => Carbon::now()->format('Y-m-d h:i:s A')
        ];
        $recently_order = self::create($request_order);
        $order_total =0;
        
        foreach($request->product as $order_detail){
            $response_product = Product::where('id',$order_detail["product_id"])->first();
            $request_products = [
                'order_id' => $recently_order->id,
                'product_id' => $order_detail['product_id'],
                'product_name' => $response_product['name'],
                'qty' => $order_detail['qty'],
                'price' => $response_product['current_price'],
                'amount' => $response_product['current_price'] * $order_detail['qty'],
            ];
            $recently_order_detail = OrderDetail::create($request_products);
            $order_total += $recently_order_detail['amount'];

            $cart = Cart::where('customer_id', auth('customers')->user()->id)
                            ->where('product_id', $order_detail["product_id"])
                            ->where('order_id',null)->first();
            if($cart){
                $cart->update([
                'order_id' => $recently_order->id
                ]);
            }
        }

        $recently_order->update([
            'total' => $order_total
        ]);

        
        return $cart;
    }

    public static function change($request, $id)
    {
        $item = self::find($id);
        
        if(!$item)
            return false;
        
        $data = $request->only(
            'user_id',
            'status',
        );
        $item->update($data);
        
        return $item;
    }



}
