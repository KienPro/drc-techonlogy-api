<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $fillable = [
        'order_id',
        'product_id',
        'product_name',
        'qty',
        'price',
        'amount'
    ];

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
