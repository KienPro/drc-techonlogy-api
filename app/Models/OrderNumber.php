<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use DB;

class OrderNumber extends Model
{

    public $timestamps = false;

    protected $fillable = [
        'shop_id', 'number'
    ];

    public static function getOrderNumber()
    {
        $item = self::where('shop_id', 1)->first();

        DB::transaction(function () use(&$item) {
            if (!$item)
                $item = OrderNumber::create(['number' => 1, 'shop_id' => 1]);
            else
                $item->increment('number');
        }, 5);

        return $item->number;
        // return '#DRC'.sprintf("%'.05d", $item->number);
    }
}
