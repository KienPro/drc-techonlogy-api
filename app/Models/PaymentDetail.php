<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Traits\BuilderWhenHelperTrait;
use App\Http\Traits\BelongToMediaTrait;
use App\Http\Traits\ModelHelperTrait;
use App\Helper\MediaHelper;
use App\Http\Traits\ListingTrait;
use App\Http\Traits\TrackUserCreateUpdate;


class PaymentDetail extends Model
{

    use SoftDeletes,
        BelongToMediaTrait,
        BuilderWhenHelperTrait,
        ListingTrait,
        TrackUserCreateUpdate,
        ModelHelperTrait;

        
    protected $fillable = [
        'amount',
        'payment_date',
        'payment_method',
        'media_id',
        'order_id',
        'note'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    
    public static function store($request, $order_id)
    {
        $data = $request->only(
            'payment_date',
            'payment_method',
            'amount',
            'note'
        );

        $data['order_id'] = $order_id;

         if($request->image)
        {
            $data['media_id'] = MediaHelper::storeImageBase64($request->image);
        }

        return self::create($data);
    }
    
     public function scopeWhenSearch($q, $search)
    {
        $q->when($search, function($q, $search) {
            $q->where('order_id', 'LIKE', '%' . $search . '%');
        });
    }

    public static function listReportAdmin($params)
    {
      return self::selectRaw('order_id, SUM(amount) as balance')
	                    ->groupBy('order_id')
                        ->with('order.customer')
                        ->whenSearch($params['search'])
                        ->sortLimitTotal($params);
    }
}
