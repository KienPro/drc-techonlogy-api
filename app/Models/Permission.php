<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'module_id', 'action_id'
    ];

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    public function action()
    {
        return $this->belongsTo(Action::class);
    }

    public function getSlugAttribute()
    {
        return $this->module->slug . '-' . $this->action->slug;
    }
}
