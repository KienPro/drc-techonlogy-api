<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Traits\BuilderWhenHelperTrait;
use App\Http\Traits\ListingTrait;
use App\Http\Traits\ModelHelperTrait;
use App\Helper\MediaHelper;
use App\Http\Traits\BelongToMediaTrait;
use DB;

class Product extends Model
{
    use SoftDeletes,
        BuilderWhenHelperTrait,
        ListingTrait,
        ModelHelperTrait,
        BelongToMediaTrait;

    protected $fillable = [
        'product_category_id',
        'product_model_id',
        'media_id',
        'secondary_media_id',
        'name',
        'price',
        'enable_status',
        'sequence',
        'cost',
        'old_price',
        'current_price',
        'is_new',
        'is_bestseller',
        'short_description',
        'long_description',
        'specification',
    ];

    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function product_category()
    {
        return $this->belongsTo(ProductCategory::class);
    }

    public function orderDetail()
    {
        return $this->belongsTo(OrderDetail::class);
    }

    public function product_model()
    {
        return $this->belongsTo(ProductModel::class);
    }

    public function secondary_media()
    {
        return $this->belongsTo(Media::class, 'secondary_media_id');
    }

    public function tags()
    {
        return $this->belongsToMany(ProductTag::class, 'relation_product_tag');
    }

    public function getTagIdsAttribute()
    {
        return DB::table('relation_product_tag')->where('product_id', $this->id)->get()->pluck('product_tag_id')->toArray();
    }

    public function getStringTagsAttribute()
    {
        return implode(', ', $this->array_tags);
    }

    public function getArrayTagsAttribute()
    {
        return $this->tags()->get(['name'])->pluck('name')->toArray();
    }

    public function scopeWhenSearch($q, $search)
    {
        $q->when($search, function($q, $search) {
            $q->where('name', 'LIKE', '%' . $search . '%');
        });
    }

    public static function listAdmin($params)
    {
        return self::with('media', 'secondary_media')
                    ->with('product_model', 'product_category')
                    ->whenSearch($params['search'])
                    ->whenWhere('enable_status', $params['enable_status'])
                    ->whenWhere('product_category_id', $params['product_category_id'])
                    ->whenWhere('product_model_id', $params['product_model_id'])
                    ->whenWhere('is_new', $params['is_new'])
                    ->whenWhere('is_bestseller', $params['is_bestseller'])
                    ->sortLimitTotal($params);
    }

    public static function listWeb($params)
    {
        return self::with('media', 'secondary_media')
                    ->with('product_model', 'product_category')                    
                    ->whenWhere('product_category_id', $params['product_category_id'])
                    ->whenWhere('product_model_id', $params['product_model_id'])
                    ->whenWhere('is_bestseller', $params['is_bestseller'])
                    ->whenSearch($params['search'])
                    ->when($params['product_tag_id'], function($q, $product_tag_id) {
                        $q->whereHas('tags', function($q) use ($product_tag_id){
                            $q->where('id', $product_tag_id);
                        });
                    })
                    ->whereBetween('current_price', [ $params['minval'], $params['maxval'] ])
                    ->active()
                    ->sortLimitTotal($params);
    }

    public static function store($request)
    {
        $data = $request->only(
            'product_model_id',
            'product_category_id',
            'name',
            'enable_status',
            'sequence',
            'cost',
            'old_price',
            'current_price',
            'is_new',
            'is_bestseller',
            'short_description',
            'long_description',
            'specification',
        );

        if($request->image)
        {
            $data['media_id'] = MediaHelper::storeImageBase64($request->image);
        }

        if($request->secondary_image)
            $data['secondary_media_id'] = MediaHelper::storeImageBase64($request->secondary_image);

        $item =  self::create($data);

        $item->tags()->sync($request->tag_ids ?? []);
        return $item;
    }

    public static function change($request, $id=null)
    {
        $item = self::find($id);

        if(!$item)
            return false;

        $data = $request->only(
            'product_model_id',
            'product_category_id',
            'name',
            'enable_status',
            'sequence',
            'cost',
            'old_price',
            'current_price',
            'is_new',
            'is_bestseller',
            'short_description',
            'long_description',
            'specification',
        );

        if($request->image)
            $data['media_id'] = MediaHelper::storeImageBase64($request->image);

        if($request->secondary_image)
            $data['secondary_media_id'] = MediaHelper::storeImageBase64($request->secondary_image);

            
        $item->update($data);

        $item->tags()->sync($request->tag_ids ?? []);

        return $item;
    }
}
