<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Traits\ListingTrait;
use App\Http\Traits\ModelHelperTrait;
use App\Helper\MediaHelper;

class ProductCategory extends Model
{
    use SoftDeletes,
        ListingTrait,
        ModelHelperTrait;

    protected $fillable = [
        'name',
        'sequence',
    ];

    public function product_models()
    {
        return $this->hasMany(ProductModel::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
    
    public function scopeWhenSearch($q, $search)
    {
        $q->when($search, function($q, $search) {
            $q->where('name', 'LIKE', '%' . $search . '%');
        });
    }

    public static function listAdmin($params)
    {
        return self::whenSearch($params['search'])
                    ->sortLimitTotal($params);
    }

    public static function listWeb($params)
    {
        return self::with('product_models')
                    ->orderBy('sequence', 'asc')
                    // ->whereHas('products', function ($query) {
                    //     $query->active();
                    // })
                    ->whenSearch($params['search'])
                    ->sortLimit($params);
    }

    public static function store($request, $id = null)
    {
        $data = $request->only(
            'name',
            'sequence',
        );

        return self::updateOrFailOrCreate($data, $id);
    }
}
