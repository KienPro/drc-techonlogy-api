<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Traits\BelongToMediaTrait;
use App\Http\Traits\BuilderWhenHelperTrait;
use App\Http\Traits\ListingTrait;
use App\Http\Traits\ModelHelperTrait;
use App\Helper\MediaHelper;

class ProductImage extends Model
{
    use SoftDeletes,
        ModelHelperTrait,
        BuilderWhenHelperTrait,
        BelongToMediaTrait,
        ListingTrait;

    protected $fillable = [
        'product_id',
        'media_id',
        'sequence'
    ];

    public static function listAdmin($params)
    {
        return self::with('media')
                    ->where('product_id', $params['product_id'])
                    ->sortLimitTotal($params);
    }

    public static function store($request)
    {
        $data = $request->only(
            'product_id',
            'sequence'
        );

        if($request->image)
            $data['media_id'] = MediaHelper::storeImageBase64($request->image);

        return self::create($data);
    }

    public static function change($request, $id)
    {
        $item = self::find($id);

        if(!$item)
            return false;

        $data = $request->only(
            'sequence'
        );

        if($request->image)
            $data['media_id'] = MediaHelper::storeImageBase64($request->image);

        return $item->update($data);
    }
}
