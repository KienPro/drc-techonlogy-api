<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Traits\ListingTrait;
use App\Http\Traits\ModelHelperTrait;
use App\Helper\MediaHelper;

class ProductModel extends Model
{
    use SoftDeletes,
        ListingTrait,
        ModelHelperTrait;

    protected $fillable = [
        'product_category_id',
        'name',
    ];

    public function product_category()
    {
        return $this->belongsTo(ProductCategory::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function scopeWhenSearch($q, $search)
    {
        $q->when($search, function($q, $search) {
            $q->where('name', 'LIKE', '%' . $search . '%');
        });
    }

    public static function listAdmin($params)
    {
        return self::with('product_category')
                    ->whenSearch($params['search'])
                    ->sortLimitTotal($params);
    }

    public static function listWeb($params)
    {
        return self::whenSearch($params['search'])
                    ->sortLimit($params);
    }

    public static function store($request, $id = null)
    {
        $data = $request->only(
            'product_category_id',
            'name',
        );

        return self::updateOrFailOrCreate($data, $id);
    }
}
