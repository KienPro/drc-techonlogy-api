<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Traits\BuilderWhenHelperTrait;
use App\Http\Traits\ListingTrait;
use App\Http\Traits\ModelHelperTrait;
use App\Http\Traits\TrackUserCreateUpdate;

class ProductTag extends Model
{
    use SoftDeletes,
        BuilderWhenHelperTrait,
        ListingTrait,
        ModelHelperTrait;

    protected $fillable = [
        'name',
        'sequence'
    ];

    public function scopeWhenSearch($q, $search)
    {
        $q->when($search, function($q, $search) {
            $q->where('name', 'LIKE', '%' . $search . '%');
        });
    }

    public static function listAdmin($params)
    {
        return self::whenSearch($params['search'])
                    ->sortLimitTotal($params);
    }

    public static function listWeb($params)
    {
        return self::whenSearch($params['search'])
                    ->sortLimit($params);
    }

    public static function store($request, $id = null)
    {
        $data = $request->only(
            'name',
            'sequence'
        );

        return self::updateOrFailOrCreate($data, $id);
    }
}
