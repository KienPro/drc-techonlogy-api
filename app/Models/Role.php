<?php

namespace App\Models;

use App\Http\Traits\BuilderWhenHelperTrait;
use App\Http\Traits\ListingTrait;
use App\Http\Traits\ModelHelperTrait;
use App\Http\Traits\TrackUserCreateUpdate;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes,
        BuilderWhenHelperTrait,
        ModelHelperTrait,
        ListingTrait,
        TrackUserCreateUpdate;

    protected $fillable = [
        'id', 'name', 'description', 'enable_status'
    ];

    protected $hidden = [
        'pivot'
    ];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'role_permissions');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_roles');
    }

    public function scopeWhenSearch($query, $search)
    {
        $query->when(filled($search), function ($query) use ($search) {
            $query->where('name', 'LIKE', '%' . $search . '%');
        });
    }

    public function getCanDeleteAttribute()
    {
        return $this->users->isEmpty();
    }

    public static function listAdmin($params)
    {
        return self::whenSearch($params['search'])
                    ->whenWhere('enable_status', $params['enable_status'])
                    ->where('id', '<>', 1)
                    ->sortLimitTotal($params);
    }

    public static function listAll()
    {
        return self::active()->get(['id', 'name']);
    }

    public static function createAdmin($request)
    {
        $role = self::create($request->only('name', 'description', 'enable_status'));

        $role->permissions()->sync($request->permissions);
    }

    public static function updateAdmin($request, $id)
    {
        $role = self::find($id);

        if(!$role) return false;

        $role->permissions()->sync($request->permissions);

        return $role->update($request->only('name', 'description', 'enable_status'));
    }
}
