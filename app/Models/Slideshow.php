<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Traits\ListingTrait;
use App\Http\Traits\ModelHelperTrait;
use App\Http\Traits\BuilderWhenHelperTrait;
use App\Http\Traits\BelongToMediaTrait;
use App\Helper\MediaHelper;

class Slideshow extends Model
{
    use SoftDeletes,
        ListingTrait,
        ModelHelperTrait,
        BuilderWhenHelperTrait,
        BelongToMediaTrait;

    protected $fillable = [
        'media_id',
        'title',
        'product_name',
        'tag_line',
        'sequence',
        'url',
        'button_name',
        'enable_status'
    ];

    public static function listAdmin($params)
    {
        return self::with('media')
                    ->whenWhere('enable_status', $params['enable_status'])
                    ->sortLimitTotal($params);
    }

    public static function listWeb($params)
    {
        return self::with('media')
                    ->orderBy('sequence', 'asc')
                    ->sortLimitTotal($params);
    }

    public static function store($request)
    {
        $data = $request->only(
            'product_name',
            'title',
            'enable_status',
            'sequence',
            'button_name',
            'url',
            'tag_line',
        );

        if($request->image)
            $data['media_id'] = MediaHelper::storeImageBase64($request->image);

        return self::create($data);
    }

    public static function updateSlideshow($request, $id)
    {
        $item = self::find($id);

        if(!$item)
            return false;
        
        $data = $request->only(
            'product_name',
            'title',
            'enable_status',
            'sequence',
            'button_name',
            'url',
            'tag_line',
        );

        if($request->image)
            $data['media_id'] = MediaHelper::storeImageBase64($request->image);

        return $item->update($data);
    }
}
