<?php

namespace App\Models;

use App\Helper\DateHelper;
use App\Helper\MediaHelper;
use App\Http\Traits\BelongToMediaTrait;
use App\Http\Traits\BuilderWhenHelperTrait;
use App\Http\Traits\ListingTrait;
use App\Http\Traits\ModelHelperTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use SoftDeletes,
        ModelHelperTrait,
        BuilderWhenHelperTrait,
        BelongToMediaTrait,
        ListingTrait,
        Notifiable,
        HasApiTokens;

    protected $fillable = [
        'id', 'first_name', 'last_name', 'username', 'email', 'password', 'gender', 'phone_number', 'address', 'enable_status',
        'media_id', 'description'
    ];

    protected $hidden = [
        'password'
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_roles');
    }

    public static function boot()
    {
        parent::boot();

        // create a event to happen on updating
        static::updating(function ($table) {
            $table->updated_by = auth('web')->user()->id ?? null;
        });

        // create a event to happen on saving
        static::creating(function ($table) {
            $table->created_by = auth('web')->user()->id ?? null;
            $table->password = bcrypt($table->password);
        });
    }

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function scopeWhenSearch($q, $search)
    {
        $q->when(filled($search), function ($q) use ($search) {
            $q->where('first_name', 'LIKE', '%' . $search . '%')
                ->orWhere('last_name', 'LIKE', '%' . $search . '%')
                ->orWhere('email', 'LIKE', '%' . $search . '%')
                ->orWhere('phone_number', 'LIKE', '%' . $search . '%')
                ->orWhere('username', 'LIKE', '%' . $search . '%')
                ->orWhereHas('roles', function ($q) use ($search) {
                    $q->where('name', $search);
                });
        });
    }

    public static function getToken($user = null)
    {
        $user = $user ?? auth('web')->user();

        return [
            'access_token' => $user->createToken(auth('web')->user()->email)->plainTextToken,
            'user' => $user->load('media')
        ];
    }

    public static function login($request)
    {
        if (auth('web')->attempt(['email' => $request['email'],
            'password' => $request['password'],
            'enable_status' => true,
            'deleted_at' => null
        ])) {
            return self::getToken();
        }

        return false;
    }

    public static function updateProfile($request)
    {
        $data = $request->only('first_name', 'last_name', 'username', 'email', 'phone_number', 'gender', 'address', 'description');

        if($request->image) {
            $data['media_id'] = MediaHelper::storeImageBase64($request->image);
        }

        auth('web')->user()->update($data);

        return auth('web')->user()->fresh('media');
    }

    public static function listAdmin($params)
    {
        return self::with([
            'roles' => function ($query) {
                $query->select('roles.id', 'roles.name');
            }
        ])
        ->whenSearch($params['search'])
        ->sortLimitTotal($params);
    }

    public static function createUser($request)
    {
        $user = User::create($request->only('first_name', 'last_name', 'username', 'email', 'gender', 'phone_number', 'address', 'enable_status', 'password', 'description') + [
                'media_id' => $request->image ? MediaHelper::storeImageBase64($request->image) : null
            ]);

        $user->roles()->sync($request->roles);

        return $user;
    }

    public static function updateUser($request)
    {
        $user = User::find($request->id);

        if(!$user) {
            return false;
        }

        $data = $request->only('first_name', 'last_name', 'username', 'email', 'gender', 'phone_number', 'address', 'enable_status', 'description');

        if($request->image) {
            $data['media_id'] = MediaHelper::storeImageBase64($request->image);
        }

        if($request->password) {
            $data['password'] = bcrypt($request->password);
        }

        $user->update($data);

        $user->roles()->sync($request->roles);

        return $user;
    }

    public static function updateUserStatus($request)
    {
        return User::where('id', $request['id'])->update($request->only('enable_status'));
    }

    public static function updatePassword($request)
    {
        $user = auth('web')->user();

        if(Hash::check($request->old_password, $user->password)) {
            return $user->update([
                'password' => bcrypt($request->password)
            ]);
        }

        return false;
    }
}
