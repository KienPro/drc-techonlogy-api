<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\BuilderWhenHelperTrait;
use App\Http\Traits\ListingTrait;
use App\Http\Traits\BelongToMediaTrait;
use App\Http\Traits\ModelHelperTrait;
class Wishlist extends Model
{
    use ListingTrait,
        BuilderWhenHelperTrait,
        BelongToMediaTrait,
        ModelHelperTrait;

    protected $fillable = [
        'product_id',
        'customer_id',
        'cart_id',
        // 'qty',
        'price',
        // 'amount',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
    
    public function product(){
        return $this->belongsTo(Product::class);
    }

    public static function listweb($params)
    {
        return Wishlist::with('product.media')
                    ->whenWhere("customer_id", auth()->user()->id)
                    ->where('cart_id', null)
                    ->orderby('id', 'desc')
                    ->sortLimitTotal($params);
                    
                    
    }

    public static function store($request)
    {
        $data = $request->only('price', 'product_id') + [
            'customer_id' => auth('customers')->user()->id,
        ];

        return self::create($data);
    }
}
