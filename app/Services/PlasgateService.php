<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;

class PlasgateService
{
    protected $method;
    protected $params;
    protected $key_param;

    public function __construct()
    {
        $this->params['gw-username'] = config('plasgate.username');
        $this->params['gw-password'] = config('plasgate.password');
        $this->params['gw-from'] = config('plasgate.from');
    }

    public function sendSms($text, $phone_number)
    {
        $this->method = 'POST';
        $this->key_param = 'form_params';
        $this->url = str_replace('{port}', config('plasgate.port'), config('plasgate.send_sms_url'));
        $this->params['gw-text'] = $text;

        if(is_string($phone_number))
            $this->params['gw-to'] = $phone_number;
        else if(is_array($phone_number))
            $this->params['gw-to'] = implode(',', $phone_number);

        return $this;
    }

    public function request($is_array = false)
    {
        try {
            $request = new Client([
                'headers' => [
                    'Accept' => 'application/json'
                ],
                'http_errors' => false,
                'verify' => false
            ]);

            $response = $request->request($this->method, $this->url, [$this->key_param => $this->params]);

            return json_decode($response->getBody(), $is_array);
        } catch (ConnectException $e) {
            return null;
        }
    }

    public function toArray()
    {
        return $this->request(true);
    }
}
