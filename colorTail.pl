#!/usr/bin/perl -w

while(<STDIN>) {
    my $line = $_;
    chomp($line);
    for($line){
        s/==>.*<==/\e[1;44m$&\e[0m/gi; #tail multiples files name in blue background
        s/{.*/\e[0;31m$&\e[0m/gi;  #java errors & stacktraces in red
        #s/info.*/\e[1;32m$&\e[0m/gi; #info replacement in green
        s/warning.*/\e[1;33m$&\e[0m/gi; #warning replacement in yellow
    }
    print $line, "\n";
}
