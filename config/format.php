<?php

return [
    'clock' => [
        'mobile' => [
            'date_time' => 'Y-m-d H:i',
            'date' => 'Y-m-d',
            'time' => 'H:i'
        ]
    ]
];
