<?php

return [
    'mobile' => json_decode(env('PUBLIC_CONTENT_PAGE_MOBILE', '{"about_us":1,"privacy_policy":2,"contact_us":3,"default":4}'), true),
];
