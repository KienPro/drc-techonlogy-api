<?php

return [
    'username' => env('PLASGATE_USERNAME'),
    'password' => env('PLASGATE_PASSWORD'),
    'port' => env('PLASGATE_PORT'),
    'from'  => env('PLASGATE_FROM'),
    'send_sms_url' => env('PLASGATE_SEND_SMS_URL')
];
