<?php

return [
    'latitude' => env('SROKSREA_LATITUDE'),
    'longitude' => env('SROKSREA_LONGITUDE'),
    'price_per_km' => env('SROKSREA_PRICE_PER_KM'),
];
