<?php

return [
    'estimate_min_per_kilo' => env('SYSTEM_ESTIMATE_MIN_PER_KILO', 5)
];
