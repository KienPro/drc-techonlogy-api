<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_category_id');
            $table->unsignedBigInteger('product_model_id');
            $table->unsignedBigInteger('media_id');
            $table->unsignedBigInteger('secondary_media_id');
            $table->string('name');
            $table->decimal('cost')->default(0);
            $table->decimal('old_price')->default(0);
            $table->decimal('current_price')->default(0);
            $table->text('short_description')->nullable();
            $table->text('long_description')->nullable();
            $table->text('specification')->nullable();
            $table->smallInteger('sequence')->default(1);
            $table->boolean('is_new')->default(1);
            $table->boolean('is_bestseller')->default(0);
            $table->boolean('enable_status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
