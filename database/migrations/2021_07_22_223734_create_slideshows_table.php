<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlideshowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slideshows', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('media_id');
            $table->string('title')->nullable();
            $table->string('product_name')->nullable();
            $table->string('tag_line')->nullable();
            $table->string('url')->nullable();
            $table->string('button_name')->nullable();
            $table->boolean('enable_status')->default(1);
            $table->smallInteger('sequence')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('slideshows', function (Blueprint $table) {
            Schema::dropIfExists('slideshows');
        });
    }
}
