<?php

use Illuminate\Database\Seeder;
use App\Models\Module;
use App\Models\Action;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;

class UserRolePermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         *  Module
         */
        Module::truncate();
        Module::insert([
            [
                'id' => 1,
                'sort' => 1,
                'category' => 'Dashboard',
                'slug' => 'dashboard'
            ], [
                'id' => 2,
                'sort' => 2,
                'category' => 'User Management',
                'slug' => 'user'
            ], [
                'id' => 3,
                'sort' => 3,
                'category' => 'User Management',
                'slug' => 'role'
            ], [
                'id' => 4,
                'sort' => 4,
                'category' => 'Order Management',
                'slug' => 'order'
            ], [
                'id' => 5,
                'sort' => 5,
                'category' => 'Product Management',
                'slug' => 'product'
            ], [
                'id' => 6,
                'sort' => 6,
                'category' => 'Product Management',
                'slug' => 'product_category'
            ], [
                'id' => 7,
                'sort' => 7,
                'category' => 'Product Management',
                'slug' => 'product_model'
            ], [
                'id' => 8,
                'sort' => 8,
                'category' => 'Product Management',
                'slug' => 'product_tag'
            ], [
                'id' => 9,
                'sort' => 9,
                'category' => 'Other Management',
                'slug' => 'slideshow'
            ], [
                'id' => 10,
                'sort' => 10,
                'category' => 'Report',
                'slug' => 'customer_report'
            ], [
                'id' => 11,
                'sort' => 11,
                'category' => 'Report',
                'slug' => 'payment_report'
            ], 
            // [
            //     'id' => 12,
            //     'sort' => 12,
            //     'category' => 'Report',
            //     'slug' => 'sale_report'
            // ]
        ]);

        /**
         *  Action
         */
        Action::truncate();
        Action::insert([
            ['slug' => 'read'],//1
            ['slug' => 'create'],//2
            ['slug' => 'update'],//3
            ['slug' => 'delete'],//4
            ['slug' => 'active'],//5
            ['slug' => 'export'],//6
            ['slug' => 'receive'],//7
            ['slug' => 'image'],//8
        ]);

        /**
         *  Permission
         */
        Permission::truncate();
        Module::find(1)->actions()->sync([1]); //dashboard
        Module::find(2)->actions()->sync([1,2,3,4]); //user
        Module::find(3)->actions()->sync([1,2,3,4]); //role
        Module::find(4)->actions()->sync([1,2,3,7]); //order
        Module::find(5)->actions()->sync([1,2,3,4,6,8]); //product
        Module::find(6)->actions()->sync([1,2,3,4]); //product category
        Module::find(7)->actions()->sync([1,2,3,4]); //product model
        Module::find(8)->actions()->sync([1,2,3,4]); // product tag
        Module::find(9)->actions()->sync([1,2,3,4]); //slideshow
        Module::find(10)->actions()->sync([1,6]); //customer
        Module::find(11)->actions()->sync([1,6]); //payment
        // Module::find(11)->actions()->sync([1]); //sale
    
        /**
         *  Admin Role
         */
        $role = Role::updateOrCreate(['id' => 1], [
                        'id' => 1,
                        'name' => 'Admin',
                        'description' => 'Admin',
                        'enable_status' => 1,
                    ]);
        $role->permissions()->sync(Permission::all()->pluck('id'));

        /**
         * Admin User
         */
        $user = User::updateOrCreate(['id' => 1], [
            'id' => 1,
            'username' => 'Admin',
            'email' => 'admin@drc.com',
            'password' => bcrypt('password')
        ]);
        $user->roles()->sync([1]);
    }
}
