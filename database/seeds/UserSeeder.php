<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'role_id' => 1,
                'media_id' => 1,
                'first_name' => 'Sruo',
                'last_name' => 'Kien',
                'username' => 'KienPro',
                'email' => 'sruo.kien@gmail.com',
                'password' => Hash::make('password'),
                'gender' => 'male',
                'description' => 'this is des',
                'phone_number' => '0965704307',
                'address' => 'Doung Ngeab lll, Phnom Penh',
                'enable_status' => 1,
                'created_at' => Carbon::now()->format('Y:m:d H:i:s')
            ]
        ]);
    }
}
