/**
* @api {post} /api/admin/auth/login 1. Login
* @apiVersion 1.0.0
* @apiName Login
* @apiGroup Authentication
*
*
* @apiParam {String} email         Description
* @apiParam {String} password      Description
*
 @apiExample {curl} Example usage:
{
	"email": "admin@gmail.com",
	"password": "1234567890"
}
*
* @apiSuccessExample  Response (example):
HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "access_token": "8|3JLvUbIRtM38R4tG389wenw8gaRJ5BQXfC4Oh7tFyOXLyJmmtCbSkVS4QCudIJFj93LRAzfRqOqEfSlC",
        "user": {
            "id": 2,
            "first_name": null,
            "last_name": null,
            "username": "Nguon Chanhout",
            "email": "chanhoutn@gmail.com",
            "gender": "male",
            "phone_number": "070 533303",
            "address": null,
            "description": "papa@admin.com",
            "enable_status": 1,
            "media_id": 45,
            "created_by": 1,
            "updated_by": null,
            "deleted_at": null,
            "created_at": "2020-05-01 00:30:49",
            "updated_at": "2020-05-01 00:30:49"
        }
    }
}
*/

/**
 * @api {post} /api/admin/auth/logout 2. Logout
 * @apiVersion 1.0.0
 * @apiName Logout
 * @apiGroup Authentication
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/auth/forgot_password 1. Forgot Password
 * @apiVersion 1.0.0
 * @apiName ForgotPassword
 * @apiGroup ForgotPassword
 *
 *
 * @apiParam {String} email              Phone number
 *
 * @apiExample {curl} Example usage:
 {
    "email": "admin@gmail.com"
}
 * @apiErrorExample  Response (example): HTTP/1.1 400 Bad
 {
    "success": false,
    "message": "Invalid phone number."
}
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
 {
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/auth/forgot_password/verify 2. Forgot Password Verify
 * @apiVersion 1.0.0
 * @apiName ForgotPasswordVerify
 * @apiGroup ForgotPassword
 *
 *
 * @apiParam {String} email              Phone number
 * @apiParam {String} verify_code              Verify Code
 *
 * @apiExample {curl} Example usage:
{
	"email": "admin@gmail.com",
	"verify_code": "l5BR"
}
 * @apiErrorExample  Response (example): HTTP/1.1 400 Bad
{
    "success": false,
    "message": "Invalid verify code"
}
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
 {
    "success": true,
    "data": {
        "token": "9cc3b0306753ae59604898c1ce1ffaab7fa543c74061bcce6f70ca2b403997f9"
    }
}
 */

/**
 * @api {post} /api/admin/auth/forgot_password/reset 3. Forgot Password Reset
 * @apiVersion 1.0.0
 * @apiName ForgotPasswordReset
 * @apiGroup ForgotPassword
 *
 *
 * @apiParam {String} email              Phone number
 * @apiParam {String} token              Token
 * @apiParam {String} password              Password
 *
 * @apiExample {curl} Example usage:
{
	"email": "admin@gmail.com",
	"token" : "95ae7f6b3f90ebb0b3579c1ea5e7277a850c4f2ba004a4986f73af8337d6ea5b",
	"password": "1234567890",
}
 * @apiErrorExample  Response (example): HTTP/1.1 400 Bad
{
    "success": false,
    "message": "Cannot not reset password."
}
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": "Reset password successfully"
}
 */

/**
 * @api {get} /api/admin/auth/user_per 3. Get User Permissions
 * @apiVersion 1.0.0
 * @apiName GetUserPermissions
 * @apiGroup Authentication
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "dashboard-read": true,
        "product-read": true,
        "product-create": true,
        "product-update": true,
        "product-delete": true,
        "supplier-read": true,
        "supplier-create": true,
        "supplier-update": true,
        "supplier-delete": true,
        "customer-read": true,
        "customer-create": true,
        "customer-update": true,
        "customer-delete": true,
        "driver-read": true,
        "order-read": true,
        "order-create": true,
        "order-update": true,
        "order-delete": true,
        "invoice-read": true,
        "invoice-create": true,
        "invoice-update": true,
        "invoice-delete": true,
        "report_sale-sale": true,
        "report_sale-customer": true,
        "report_sale-product": true,
        "slide_show-read": true,
        "slide_show-create": true,
        "slide_show-update": true,
        "slide_show-delete": true,
        "product_category-read": true,
        "product_category-create": true,
        "product_category-update": true,
        "product_category-delete": true,
        "supplier_category-read": true,
        "supplier_category-create": true,
        "supplier_category-update": true,
        "supplier_category-delete": true,
        "customer_group-read": true,
        "customer_group-create": true,
        "customer_group-update": true,
        "customer_group-delete": true,
        "branch-read": true,
        "branch-create": true,
        "branch-update": true,
        "branch-delete": true,
        "role-read": true,
        "role-create": true,
        "role-update": true,
        "role-delete": true,
        "product_group-read": true,
        "product_group-create": true,
        "product_group-update": true,
        "product_group-delete": true,
        "product_lable-read": true,
        "product_lable-create": true,
        "product_lable-update": true,
        "product_lable-delete": true,
        "user-read": true,
        "user-create": true,
        "user-update": true,
        "user-delete": true,
        "content-read": true,
        "content-create": true,
        "content-update": true,
        "content-delete": true,
        "unit_of_measure-read": true,
        "unit_of_measure-create": true,
        "unit_of_measure-update": true,
        "unit_of_measure-delete": true
    }
}
 */
