/**
 * @api {get} /api/admin/comments/list 1. List Comment
 * @apiVersion 1.0.0
 * @apiName ListComment
 * @apiGroup Comment
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {Boolean} [is_hide]
 * @apiParam {Integer} from_id          is 'prodcut_id' || 'post_id'
 * @apiParam {String} from_type         valide values 'prodcut' || 'post'
 * @apiParam {String} [search]          by 'comment' || 'customer.name' || 'customer.phone_number'
 * @apiParam {any_string} [export]
 * @apiParam {Integer} [offset]
 * @apiParam {Integer} [limit]
 * @apiParam {String} [order]     eg. order=updated_at
 * @apiParam {String} [sort]     eg. sort=asc|desc
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "list": [
            {
                "id": 1,
                "comment": "Yummy",
                "amount_likes": 1,
                "created_at": "2021-03-03T07:00:35.000000Z",
                "hide_at": "2021-03-03 14:52:31",
                "customer": {
                    "id": 1,
                    "name": "Sokchea",
                    "phone_number": "+855966523287",
                    "media": null
                }
            }
        ],
        "total": 1,
        "total_hide": "1",
    }
}
 */

/**
 * @api {post} /api/admin/comments/hide/{id} 2. Hide Comment
 * @apiVersion 1.0.0
 * @apiName HideComment
 * @apiGroup Comment
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {Boolean} [is_hide]
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {get} /api/admin/comments/list/preview 3. List Preview Comment
 * @apiVersion 1.0.0
 * @apiName ListPreviewComment
 * @apiGroup Comment
 *
 * @apiParam {Integer} from_id          is 'prodcut_id' || 'post_id'
 * @apiParam {String} from_type         valide values 'prodcut' || 'post'
 * @apiParam {any_string} [export]
 * @apiParam {Integer} [offset]
 * @apiParam {Integer} [limit]
 * @apiParam {String} [order]     eg. order=updated_at
 * @apiParam {String} [sort]     eg. sort=asc|desc
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "list": [
            {
                "id": 1,
                "comment": "Yummy",
                "amount_likes": 1,
                "created_at": "2021-03-03T07:00:35.000000Z",
                "customer": {
                    "id": 1,
                    "name": "Sokchea",
                    "media": null
                }
            }
        ],
        "total": 1
    }
}
 */
