/**
 * @api {get} /api/admin/customers/list 1. List Customer
 * @apiVersion 1.0.0
 * @apiName ListCustomer
 * @apiGroup Customer
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {String} [platform]
 * @apiParam {Integer} [customer_group_id]
 * @apiParam {String} [search]          search by 'name'|'phone_number'|'email'
 * @apiParam {any_string} [export]
 * @apiParam {Integer} [offset]
 * @apiParam {Integer} [limit]
 * @apiParam {String} [order]     eg. order=updated_at
 * @apiParam {String} [sort]     eg. sort=asc|desc
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "list": [
            {
                "id": 2,
                "name": "Kilogram (kg)",
                "customer_group_id": 1,
                "phone_number": "012345678",
                "other_phone": "012345678",
                "email": null,
                "address": null,
                "media_id": 20,
                "platform": "admin",
                "enable_status": 1,
                "deleted_at": null,
                "created_at": "2020-11-19 21:25:05",
                "updated_at": "2020-11-19 21:25:05",
                "media": {
                    "id": 20,
                    "url": "http://localhost:8081/uploads/images/657ac1e7ca05cc2f928667afb86eea39.png",
                    "name": "657ac1e7ca05cc2f928667afb86eea39.png",
                    "created_at": "2020-11-19 21:25:05",
                    "updated_at": "2020-11-19 21:25:05"
                },
                "customer_group": {
                    "id": 1,
                    "name": "Kilogram (kg)",
                    "deleted_at": null,
                    "created_at": "2020-11-19 21:24:25",
                    "updated_at": "2020-11-19 21:24:25"
                }
            }
        ],
        "total": 1
    }
}
 */

/**
 * @api {get} /api/admin/customers/list_all 0. List All Customer
 * @apiVersion 1.0.0
 * @apiName ListAllCustomer
 * @apiGroup Customer
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": [
        {
            "id": 1,
            "name": "A001"
        }
    ]
}
 */

/**
 * @api {post} /api/admin/customers/create 2. Create Customer
 * @apiVersion 1.0.0
 * @apiName CreateCustomer
 * @apiGroup Customer
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {String} name
 * @apiParam {Integer} [customer_group_id]
 * @apiParam {String} phone_number
 * @apiParam {String} [other_phone]
 * @apiParam {String} [email]
 * @apiParam {String} [address]
 * @apiParam {Boolean} [enable_status]
 * @apiParam {ImageBase64} [image]
 * @apiParam {String} [password]
 *
 @apiExample {curl} Example usage:
{
    "name": "Customer A001",
    "customer_group_id": 1,
    "phone_number": "0966523286",
    "other_phone": "0966523286/012602201",
    "email": "sokcheavan3@gmail.com",
    "address": "Phnom Penh",
    "enable_status": 1,
    "image": "{{image_base64}}",
    "password": "password"
}
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "id": 3,
        "name": "Kilogram (kg)"
    }
}
 */

/**
 * @api {get} /api/admin/customers/show/{id} 3. Get Customer detail
 * @apiVersion 1.0.0
 * @apiName GetCustomerDetail
 * @apiGroup Customer
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "id": 1,
        "name": "Customer A001",
        "customer_group_id": 1,
        "phone_number": "0966523286",
        "other_phone": "0966523286/012602201",
        "email": "sokcheavan3@gmail.com",
        "address": "Phnom Penh",
        "enable_status": 1,
        "media": {
            "id": 1,
            "url": "http://localhost:8081/uploads/images/83d7b7467e7263494fa8f46de853d739.png",
            "name": "83d7b7467e7263494fa8f46de853d739.png",
            "created_at": "2020-11-08 13:30:47",
            "updated_at": "2020-11-08 13:30:47"
        }
    }
}
 */

/**
 * @api {post} /api/admin/customers/update/{id} 4. Update Customer
 * @apiVersion 1.0.0
 * @apiName UpdateCustomer
 * @apiGroup Customer
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {String} name
 * @apiParam {Integer} [customer_group_id]
 * @apiParam {String} phone_number
 * @apiParam {String} [other_phone]
 * @apiParam {String} [email]
 * @apiParam {String} [address]
 * @apiParam {Boolean} [enable_status]
 * @apiParam {ImageBase64} [image]
 * @apiParam {String} [password]
 *
 @apiExample {curl} Example usage:
{
    "name": "Customer A001",
    "customer_group_id": 1,
    "phone_number": "0966523286",
    "other_phone": "0966523286/012602201",
    "email": "sokcheavan3@gmail.com",
    "address": "Phnom Penh",
    "enable_status": 1,
    "image": "{{image_base64}}",
    "password": "password"
}
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/customers/status/{id} 5. Update Customer status
 * @apiVersion 1.0.0
 * @apiName UpdateCustomerStatus
 * @apiGroup Customer
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {boolean} enable_status
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
 {
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/customers/delete/{id} 6. Delete Customer
 * @apiVersion 1.0.0
 * @apiName DeleteCustomer
 * @apiGroup Customer
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */
