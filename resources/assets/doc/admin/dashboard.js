/**
 * @api {get} /api/admin/dashboard Dashboard
 * @apiVersion 1.0.0
 * @apiName Dashboard
 * @apiGroup Dashboard
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {String} date      	Date | eg. today, weekly, monthly, yearly, all | default is all
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "total_restaurants": 6,
        "total_posts": 1,
        "total_customers": 1,
        "total_users": 1
    }
}
 */
