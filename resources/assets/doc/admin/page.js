/**
 * @api {get} /api/admin/pages/list 1. List Page
 * @apiVersion 1.0.0
 * @apiName ListPage
 * @apiGroup Page
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {String} [search]
 * @apiParam {any_string} [export]
 * @apiParam {Integer} [offset]
 * @apiParam {Integer} [limit]
 * @apiParam {String} [order]     eg. order=updated_at
 * @apiParam {String} [sort]     eg. sort=asc|desc
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "list": [
            {
                "id": 1,
                "title_en": "About Us",
                "title_kh": "អំពីយើងខ្ញុំ",
                "short_description_en": "About Us",
                "short_description_kh": "អំពីយើងខ្ញុំ",
                "media_id": 61,
                "enable_status": 1,
                "deleted_at": null,
                "created_by": 1,
                "updated_by": null,
                "created_at": "2020-02-08 15:54:33",
                "updated_at": "2020-02-08 15:54:33",
                "media": {
                    "id": 61,
                    "media_id": 61,
                    "file_url": "http://localhost:8081/uploads/images/0f57885859a872af20976d08d66609de.png",
                    "file_name": "0f57885859a872af20976d08d66609de.png",
                    "size": "Original"
                }
            }
        ],
        "total": 1
    }
}
 */

/**
 * @api {post} /api/admin/pages/create 2. Create Page
 * @apiVersion 1.0.0
 * @apiName CreatePage
 * @apiGroup Page
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {string} title_en
 * @apiParam {string} title_kh
 * @apiParam {string} [short_description_en]
 * @apiParam {string} [short_description_kh]
 * @apiParam {String} image         Base 64
 * @apiParam {boolean} [enable_status]
 *
 @apiExample {curl} Example usage:
{
	"title_en": "About Us",
	"title_kh": "អំពីយើងខ្ញុំ",
	"short_description_en": "About Us",
	"short_description_kh": "អំពីយើងខ្ញុំ",
	"enable_status": 1,
	"image": "data:image/jpeg;base64,.........."
}
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
 {
    "success": true,
    "data": null
}
 */

/**
 * @api {get} /api/admin/pages/show/{id} 3. Get Page detail
 * @apiVersion 1.0.0
 * @apiName GetPageDetail
 * @apiGroup Page
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "id": 1,
        "title_en": "About Us",
        "title_kh": "អំពីយើងខ្ញុំ",
        "short_description_en": "About Us",
        "short_description_kh": "អំពីយើងខ្ញុំ",
        "enable_status": 1,
        "media": {
            "id": 61,
            "media_id": 61,
            "file_url": "http://localhost:8081/uploads/images/0f57885859a872af20976d08d66609de.png",
            "file_name": "0f57885859a872af20976d08d66609de.png",
            "size": "Original"
        }
    }
}
 */

/**
 * @api {post} /api/admin/pages/update/{id} 4. Update Page
 * @apiVersion 1.0.0
 * @apiName UpdatePage
 * @apiGroup Page
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {string} title_en
 * @apiParam {string} title_kh
 * @apiParam {string} [short_description_en]
 * @apiParam {string} [short_description_kh]
 * @apiParam {String} [image]         Base 64
 * @apiParam {boolean} [enable_status]
 *
 @apiExample {curl} Example usage:
{
	"title_en": "About Us",
	"title_kh": "អំពីយើងខ្ញុំ",
	"short_description_en": "About Us",
	"short_description_kh": "អំពីយើងខ្ញុំ",
	"enable_status": 1,
	"image": "data:image/jpeg;base64,.........."
}
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/pages/status/{id} 5. Update Page status
 * @apiVersion 1.0.0
 * @apiName UpdatePageStatus
 * @apiGroup Page
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {boolean} enable_status
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/pages/delete/{id} 6. Delete Page
 * @apiVersion 1.0.0
 * @apiName DeletePage
 * @apiGroup Page
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */
