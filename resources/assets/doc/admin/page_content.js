/**
 * @api {get} /api/admin/page_contents/list 1. List PageContent
 * @apiVersion 1.0.0
 * @apiName ListPageContent
 * @apiGroup PageContent
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {Integer} page_id
 * @apiParam {String} [order]     eg. order=updated_at
 * @apiParam {String} [sort]     eg. sort=asc|desc
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        {
            "id": 1,
            "content_type": "image",
            "title_en": null,
            "title_kh": null,
            "text_en": null,
            "text_kh": null,
            "video_url": null,
            "sequence": null,
            "media": {
                "id": 64,
                "media_id": 64,
                "file_url": "http://localhost:8081/uploads/images/b5fca61770edf66da52d9cf43d3cebcc.png",
                "file_name": "b5fca61770edf66da52d9cf43d3cebcc.png",
                "size": "Original"
            }
        }
    }
}
 */

/**
 * @api {post} /api/admin/page_contents/create 2. Create PageContent
 * @apiVersion 1.0.0
 * @apiName CreatePageContent
 * @apiGroup PageContent
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {integer} page_id
 * @apiParam {string} content_type
 * @apiParam {string} [title_en]
 * @apiParam {string} [title_kh]
 * @apiParam {string} [text_en]
 * @apiParam {string} [text_kh]
 * @apiParam {string} [video_url]
 * @apiParam {String} [image]         Base 64
 * @apiParam {integer} [sequence]
 *
 @apiExample {curl} Example Image:
{
	"page_id": 1,
	"content_type": "image",
	"image": "Base 64",
	"sequence": 1
}
 @apiExample {curl} Example usage:
{
	"page_id": 1,
	"content_type": "text",
	"title_en": "About us",
	"title_kh": "អំពីយើងខ្ញុំ",
	"text_en": "About us",
	"text_kh": "អំពីយើងខ្ញុំ"
}
 @apiExample {curl} Example usage:
{
	"page_id": 1,
	"content_type": "video",
	"video_url": "https://www.youtube.com/watch?v=XdSLJnnRDN4"
}
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "id": 5,
        "content_type": "text",
        "title_en": "About us",
        "title_kh": "អំពីយើងខ្ញុំ",
        "text_en": "About us",
        "text_kh": "អំពីយើងខ្ញុំ",
        "video_url": "https://www.youtube.com/watch?v=XdSLJnnRDN4",
        "sequence": 1,
        "media": {
            "id": 64,
            "media_id": 64,
            "file_url": "http://localhost:8081/uploads/images/b5fca61770edf66da52d9cf43d3cebcc.png",
            "file_name": "b5fca61770edf66da52d9cf43d3cebcc.png",
            "size": "Original"
        }
    }
}
 */

/**
 * @api {post} /api/admin/page_contents/update/{id} 3. Update PageContent
 * @apiVersion 1.0.0
 * @apiName UpdatePageContent
 * @apiGroup PageContent
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {string} content_type
 * @apiParam {string} [title_en]
 * @apiParam {string} [title_kh]
 * @apiParam {string} [text_en]
 * @apiParam {string} [text_kh]
 * @apiParam {string} [video_url]
 * @apiParam {String} [image]         Base 64
 * @apiParam {integer} [sequence]
 *
 @apiExample {curl} Example Image:
{
	"content_type": "image",
	"image": "Base 64",
	"sequence": 1
}
 @apiExample {curl} Example text:
{
	"content_type": "text",
	"title_en": "About us",
	"title_kh": "អំពីយើងខ្ញុំ",
	"text_en": "About us",
	"text_kh": "អំពីយើងខ្ញុំ"
}
 @apiExample {curl} Example video:
{
	"content_type": "video",
	"video_url": "https://www.youtube.com/watch?v=XdSLJnnRDN4"
}
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "id": 5,
        "content_type": "text",
        "title_en": "About us",
        "title_kh": "អំពីយើងខ្ញុំ",
        "text_en": "About us",
        "text_kh": "អំពីយើងខ្ញុំ",
        "video_url": "https://www.youtube.com/watch?v=XdSLJnnRDN4",
        "sequence": 1,
        "media": {
            "id": 64,
            "media_id": 64,
            "file_url": "http://localhost:8081/uploads/images/b5fca61770edf66da52d9cf43d3cebcc.png",
            "file_name": "b5fca61770edf66da52d9cf43d3cebcc.png",
            "size": "Original"
        }
    }
}
 */

/**
 * @api {post} /api/admin/page_contents/delete/{id} 4. Delete PageContent
 * @apiVersion 1.0.0
 * @apiName DeletePageContent
 * @apiGroup PageContent
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */
