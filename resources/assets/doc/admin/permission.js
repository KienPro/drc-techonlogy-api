/**
 * @api {get} /api/admin/permissions/list_all 0. List All Permission
 * @apiVersion 1.0.0
 * @apiName ListAllPermission
 * @apiGroup Permission
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "dashboard": [
            {
                "id": 1,
                "action": "read"
            }
        ],
        "user": [
            {
                "id": 2,
                "action": "read"
            },
        ]
    }
}
 */
