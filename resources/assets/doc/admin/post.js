/**
 * @api {get} /api/admin/posts/list 01. List Post
 * @apiVersion 1.0.0
 * @apiName ListPost
 * @apiGroup Post
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {Integer} [restaurant_id]
 * @apiParam {String} [search]
 * @apiParam {any_string} [export]
 * @apiParam {Integer} [offset]
 * @apiParam {Integer} [limit]
 * @apiParam {String} [order]     eg. order=updated_at
 * @apiParam {String} [sort]     eg. sort=asc|desc
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "list": [
            {
                "id": 1,
                "restaurant_id": 1,
                "media_id": 1,
                "text": "About Us",
                "amount_views": 0,
                "amount_likes": 0,
                "amount_comments": 0,
                "amount_share": 0,
                "amount_copy_link": 0,
                "amount_search": 0,
                "enable_status": 1,
                "created_by": 1,
                "updated_by": 1,
                "deleted_at": null,
                "created_at": "2021-02-15 13:36:11",
                "updated_at": "2021-02-26 10:40:03",
                "publish_at": "2021-02-26 10:40:03",
                "push_notification_at": "2021-02-26 10:39:14",
                "media": {
                    "id": 1,
                    "url": "http://localhost:7081/uploads/images/3a7575e244da2cfc072e2ab84b0c96a7.png",
                    "name": "3a7575e244da2cfc072e2ab84b0c96a7.png",
                    "created_at": "2021-02-15 13:36:11",
                    "updated_at": "2021-02-15 13:36:11"
                },
                "restaurant": {
                    "id": 1,
                    "name_en": "Test"
                }
            }
        ],
        "total": 1
    }
}
 */

/**
 * @api {post} /api/admin/posts/create 02. Create Post
 * @apiVersion 1.0.0
 * @apiName CreatePost
 * @apiGroup Post
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {string} text
 * @apiParam {String} image         Base 64
 * @apiParam {Integer} restaurant_id
 * @apiParam {boolean} [enable_status]
 *
 @apiExample {curl} Example usage:
{
	"text": "About Us",
	"enable_status": 1,
    "restaurant_id": 1,
	"image": "data:image/jpeg;base64,.........."
}
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
 {
    "success": true,
    "data": null
}
 */

/**
 * @api {get} /api/admin/posts/show/{id} 03. Get Post detail
 * @apiVersion 1.0.0
 * @apiName GetPostDetail
 * @apiGroup Post
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "id": 1,
        "text": "About Us",
        "restaurant_id": 1,
        "enable_status": 1,
        "publish_at": "2021-02-26 10:40:03",
        "media": {
            "id": 1,
            "url": "http://localhost:7081/uploads/images/3a7575e244da2cfc072e2ab84b0c96a7.png",
            "name": "3a7575e244da2cfc072e2ab84b0c96a7.png",
            "created_at": "2021-02-15 13:36:11",
            "updated_at": "2021-02-15 13:36:11"
        }
    }
}
 */

/**
 * @api {post} /api/admin/posts/update/{id} 04. Update Post
 * @apiVersion 1.0.0
 * @apiName UpdatePost
 * @apiGroup Post
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {string} text
 * @apiParam {String} [image]         Base 64
 * @apiParam {Integer} restaurant_id
 * @apiParam {boolean} [enable_status]
 *
 @apiExample {curl} Example usage:
{
	"text": "About Us",
	"enable_status": 1,
    "restaurant_id": 1,
	"image": null
}
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/posts/status/{id} 05. Update Post status
 * @apiVersion 1.0.0
 * @apiName UpdatePostStatus
 * @apiGroup Post
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {boolean} enable_status
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/posts/delete/{id} 06. Delete Post
 * @apiVersion 1.0.0
 * @apiName DeletePost
 * @apiGroup Post
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/posts/push_notification/{id} 07. Post Push Notification
 * @apiVersion 1.0.0
 * @apiName PostPushNotification
 * @apiGroup Post
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/posts/publish/{id} 08. Publish Post
 * @apiVersion 1.0.0
 * @apiName PublishPost
 * @apiGroup Post
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {get} /api/admin/posts/social/{id} 09. Social Post detail
 * @apiVersion 1.0.0
 * @apiName PostSocialDetail
 * @apiGroup Post
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "id": 1,
        "text": "About Us",
        "publish_at": "2021-02-26 10:40:03",
        "amount_likes": 0,
        "amount_comments": 0,
        "amount_share": 0,
        "amount_views": 0,
        "media": {
            "id": 1,
            "url": "http://localhost:7081/uploads/images/3a7575e244da2cfc072e2ab84b0c96a7.png",
            "name": "3a7575e244da2cfc072e2ab84b0c96a7.png",
            "created_at": "2021-02-15 13:36:11",
            "updated_at": "2021-02-15 13:36:11"
        },
        "restaurant": {
            "id": 1,
            "name_en": "Test",
            "media": {
                "id": 4,
                "url": "http://localhost:7081/uploads/images/e60059995fd3c7d615e9a00ce428d1c0.png",
                "name": "e60059995fd3c7d615e9a00ce428d1c0.png",
                "created_at": "2021-02-16 13:23:05",
                "updated_at": "2021-02-16 13:23:05"
            }
        }
    }
}
 */

/**
 * @api {get} /api/admin/posts/preview/{id} 10. Preview Post
 * @apiVersion 1.0.0
 * @apiName PreviewPost
 * @apiGroup Post
 *
 * @apiHeader {String} [Authorization] Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "id": 1,
        "text": "About Us",
        "amount_views": 0,
        "amount_likes": 0,
        "amount_comments": 0,
        "amount_share": 0,
        "publish_at": "2021-02-26 10:40:03",
        "image": "http://localhost:7081/uploads/images/3a7575e244da2cfc072e2ab84b0c96a7.png",
        "restaurant": {
            "id": 1,
            "name": null,
            "profile": "http://localhost:7081/uploads/images/e60059995fd3c7d615e9a00ce428d1c0.png"
        },
        "contents": [
            {
                "id": 2,
                "content_type": "text",
                "text": "About us1"
            },
            {
                "id": 3,
                "content_type": "video",
                "video_url": "https://www.youtube.com/watch?v=9DHuHkwaWWs"
            },
            {
                "id": 4,
                "content_type": "image",
                "image": "http://localhost:7081/uploads/images/c3937057dac525aeda9e9a05babb0c5d.png"
            }
        ]
    }
}
 */
