/**
 * @api {get} /api/admin/post_contents/list 1. List PostContent
 * @apiVersion 1.0.0
 * @apiName ListPostContent
 * @apiGroup PostContent
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {Integer} post_id
 * @apiParam {String} [order]     eg. order=updated_at
 * @apiParam {String} [sort]     eg. sort=asc|desc
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        {
            "id": 1,
            "content_type": "image",
            "text": "About us1",
            "video_url": null,
            "sequence": null,
            "media": {
                "id": 64,
                "media_id": 64,
                "file_url": "http://localhost:8081/uploads/images/b5fca61770edf66da52d9cf43d3cebcc.png",
                "file_name": "b5fca61770edf66da52d9cf43d3cebcc.png",
                "size": "Original"
            }
        }
    }
}
 */

/**
 * @api {post} /api/admin/post_contents/create 2. Create PostContent
 * @apiVersion 1.0.0
 * @apiName CreatePostContent
 * @apiGroup PostContent
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {integer} post_id
 * @apiParam {string} content_type
 * @apiParam {string} [text]
 * @apiParam {string} [video_url]
 * @apiParam {String} [image]         Base 64
 * @apiParam {integer} [sequence]
 *
 @apiExample {curl} Example Image:
{
	"post_id": 1,
	"content_type": "image",
	"image": "Base 64",
	"sequence": 1
}
 @apiExample {curl} Example Text:
{
	"post_id": 1,
	"content_type": "text",
	"text": "About us"
}
 @apiExample {curl} Example Video:
{
	"post_id": 1,
	"content_type": "video",
	"video_url": "https://www.youtube.com/watch?v=XdSLJnnRDN4"
}
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/post_contents/update/{id} 3. Update PostContent
 * @apiVersion 1.0.0
 * @apiName UpdatePostContent
 * @apiGroup PostContent
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {string} content_type
 * @apiParam {string} [text]
 * @apiParam {string} [video_url]
 * @apiParam {String} [image]         Base 64
 * @apiParam {integer} [sequence]
 *
 @apiExample {curl} Example Image:
{
	"content_type": "image",
	"image": "Base 64",
	"sequence": 1
}
 @apiExample {curl} Example text:
{
	"content_type": "text",
	"text": "About us",
	"sequence": 1
}
 @apiExample {curl} Example video:
{
	"content_type": "video",
	"video_url": "https://www.youtube.com/watch?v=XdSLJnnRDN4",
	"sequence": 1
}
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/post_contents/delete/{id} 4. Delete PostContent
 * @apiVersion 1.0.0
 * @apiName DeletePostContent
 * @apiGroup PostContent
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */
