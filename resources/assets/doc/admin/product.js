/**
 * @api {get} /api/admin/products/list 1. List Product
 * @apiVersion 1.0.0
 * @apiName ListProduct
 * @apiGroup Product
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {Integer} [product_category_id]
 * @apiParam {Integer} [restaurant_id]
 * @apiParam {Integer} [enable_status]
 * @apiParam {String} [search]          search by 'name_en' | 'name_kh'
 * @apiParam {any_string} [export]
 * @apiParam {Integer} [offset]
 * @apiParam {Integer} [limit]
 * @apiParam {String} [order]     eg. order=updated_at
 * @apiParam {String} [sort]     eg. sort=asc|desc
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "list": [
            {
                "id": 1,
                "restaurant_id": 1,
                "name_en": "Test",
                "name_kh": "Test",
                "product_category_id": 1,
                "price": 123,
                "short_description": "Test",
                "long_description": "Test",
                "amount_views": 0,
                "amount_likes": 0,
                "amount_comments": 0,
                "amount_share": 0,
                "amount_copy_link": 0,
                "amount_search": 0,
                "sequence": 1,
                "enable_status": 1,
                "deleted_at": null,
                "created_at": "2021-02-17 10:09:03",
                "updated_at": "2021-02-17 10:09:03",
                "media": {
                    "id": 7,
                    "url": "http://localhost:7081/uploads/images/c424feab597c4a28da70c7deae563391.png",
                    "name": "c424feab597c4a28da70c7deae563391.png",
                    "created_at": "2021-02-17 11:27:42",
                    "updated_at": "2021-02-17 11:27:42"
                },
                "restaurant": {
                    "id": 1,
                    "name_en": "Test",
                    "name_kh": "Test",
                    "latitude": "14.12741234",
                    "longitude": "104.12741234",
                    "webiste_url": "fb.com",
                    "email": "test@papadeliver.com",
                    "phone_number": "0966523286",
                    "other_phone_number": "0966523286 / 0966523286",
                    "address": "Test",
                    "about": "Test",
                    "media_id": 4,
                    "banner_media_id": 5,
                    "restaurant_category_id": 1,
                    "amount_views": 0,
                    "amount_likes": 0,
                    "amount_comments": 0,
                    "amount_share": 0,
                    "amount_copy_link": 0,
                    "amount_search": 0,
                    "enable_status": 1,
                    "sequence": 1,
                    "deleted_at": null,
                    "created_at": "2021-02-16 13:23:05",
                    "updated_at": "2021-02-16 13:23:05"
                },
                "product_category": {
                    "id": 1,
                    "restaurant_id": 1,
                    "name_en": "Kilogram (kg)",
                    "name_kh": "Kilogram (kg)",
                    "sequence": 1,
                    "enable_status": 1,
                    "deleted_at": null,
                    "created_at": "2021-02-16 18:13:45",
                    "updated_at": "2021-02-16 18:13:45"
                }
            }
        ],
        "total": 1
    }
}
 */

/**
 * @api {get} /api/admin/products/list_all 0. List All Product
 * @apiVersion 1.0.0
 * @apiName ListAllProduct
 * @apiGroup Product
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": [
        {
            "id": 1,
            "name": "Kilogram (kg)"
        }
    ]
}
 */

/**
 * @api {post} /api/admin/products/create 2. Create Product
 * @apiVersion 1.0.0
 * @apiName CreateProduct
 * @apiGroup Product
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {integer} restaurant_id
 * @apiParam {String} name_en
 * @apiParam {String} name_kh
 * @apiParam {integer} product_category_id
 * @apiParam {float} price
 * @apiParam {String} short_description
 * @apiParam {Text} long_description
 * @apiParam {Base64} image
 * @apiParam {Boolean} [enable_status]
 * @apiParam {integer} [sequence]
 *
 @apiExample {curl} Example usage:
{
    "name_en": "Test",
    "name_kh": "Test",
    "restaurant_id": 1,
    "product_category_id": 1,
    "price": 123,
    "short_description": "Test",
    "long_description": "Test",
    "enable_status": 1,
    "sequence": 1,
    "image": "{{image_base64}}"
}
 *
 * @apiSuccessExample  Response (example):
{
    "success": true,
    "data": null
}
 */

/**
 * @api {get} /api/admin/products/show/{id} 3. Get Product detail
 * @apiVersion 1.0.0
 * @apiName GetProductDetail
 * @apiGroup Product
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "id": 1,
        "restaurant_id": 1,
        "name_en": "Test",
        "name_kh": "Test",
        "product_category_id": 1,
        "price": "123.00",
        "short_description": "Test",
        "long_description": "Test",
        "enable_status": 1,
        "sequence": 1
    }
}
 */

/**
 * @api {post} /api/admin/products/update/{id} 4. Update Product
 * @apiVersion 1.0.0
 * @apiName UpdateProduct
 * @apiGroup Product
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {integer} restaurant_id
 * @apiParam {String} name_en
 * @apiParam {String} name_kh
 * @apiParam {integer} product_category_id
 * @apiParam {float} price
 * @apiParam {String} short_description
 * @apiParam {Base64} [image]
 * @apiParam {Text} long_description
 * @apiParam {Boolean} [enable_status]
 * @apiParam {integer} [sequence]
 *
 @apiExample {curl} Example usage:
{
    "name_en": "Test",
    "name_kh": "Test",
    "restaurant_id": 1,
    "product_category_id": 1,
    "price": 123,
    "short_description": "Test",
    "long_description": "Test",
    "enable_status": 1,
    "sequence": 1,
    "image": "{{image_base64}}"
}
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/products/status/{id} 5. Update Product status
 * @apiVersion 1.0.0
 * @apiName UpdateProductStatus
 * @apiGroup Product
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {boolean} enable_status
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
 {
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/products/sequence/{id} 6. Update Product Sequence
 * @apiVersion 1.0.0
 * @apiName UpdateProductSequence
 * @apiGroup Product
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {integer} sequence
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/products/delete/{id} 7. Delete Product
 * @apiVersion 1.0.0
 * @apiName DeleteProduct
 * @apiGroup Product
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {get} /api/admin/products/social/{id} 8. Social Product detail
 * @apiVersion 1.0.0
 * @apiName ProductSocialDetail
 * @apiGroup Product
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "id": 4,
        "restaurant_id": 1,
        "restaurant_name": "Test",
        "name_en": "Test",
        "amount_likes": 1,
        "amount_comments": 2,
        "amount_share": 0,
        "amount_views": 0,
        "media": null
    }
}
 */

/**
 * @api {get} /api/admin/products/preview/{id} 9. Preview Product
 * @apiVersion 1.0.0
 * @apiName PreviewProduct
 * @apiGroup Product
 *
 * @apiHeader {String} [Authorization] Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "id": 1,
        "price": 123,
        "short_description": "Test",
        "long_description": "Test",
        "amount_likes": 0,
        "amount_comments": 0,
        "amount_share": 0,
        "name": "Test",
        "category": "Kilogram (kg)",
        "variants": [
            {
                "id": 1,
                "name": "testing",
                "values": []
            },
            {
                "id": 3,
                "name": "testing",
                "values": [
                    "value testing 1",
                    "value testing 2"
                ]
            }
        ],
        "restaurant": {
            "id": 1,
            "name": "Test",
            "image": "http://localhost:7081/uploads/images/e60059995fd3c7d615e9a00ce428d1c0.png"
        },
        "images": [
            "http://localhost:7081/uploads/images/c424feab597c4a28da70c7deae563391.png"
        ]
    }
}
 */
