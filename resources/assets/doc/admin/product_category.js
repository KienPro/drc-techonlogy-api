/**
 * @api {get} /api/admin/product_categories/list 1. List ProductCategory
 * @apiVersion 1.0.0
 * @apiName ListProductCategory
 * @apiGroup ProductCategory
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {Integer} [restaurant_id]
 * @apiParam {String} [search]          search by 'name_en' | 'name_kh'
 * @apiParam {any_string} [export]
 * @apiParam {Integer} [offset]
 * @apiParam {Integer} [limit]
 * @apiParam {String} [order]     eg. order=updated_at
 * @apiParam {String} [sort]     eg. sort=asc|desc
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "list": [
            {
                "id": 1,
                "restaurant_id": 1,
                "name_en": "Kilogram (kg)",
                "name_kh": "Kilogram (kg)",
                "sequence": 2,
                "enable_status": 1,
                "deleted_at": null,
                "created_at": "2021-01-31 22:54:26",
                "updated_at": "2021-01-31 22:54:26",
                "restaurant": {
                    "id": 1,
                    "name_en": "Test",
                    "name_kh": "Test",
                    "latitude": "14.12741234",
                    "longitude": "104.12741234",
                    "webiste_url": "fb.com",
                    "email": "test@papadeliver.com",
                    "phone_number": "0966523286",
                    "other_phone_number": "0966523286 / 0966523286",
                    "address": "Test",
                    "about": "Test",
                    "media_id": 4,
                    "banner_media_id": 5,
                    "restaurant_category_id": 1,
                    "amount_views": 0,
                    "amount_likes": 0,
                    "amount_comments": 0,
                    "amount_share": 0,
                    "amount_copy_link": 0,
                    "amount_search": 0,
                    "enable_status": 1,
                    "sequence": 1,
                    "deleted_at": null,
                    "created_at": "2021-02-16 13:23:05",
                    "updated_at": "2021-02-16 13:23:05"
                }
            }
        ],
        "total": 1
    }
}
 */

/**
 * @api {get} /api/admin/product_categories/list_all 0. List All ProductCategory
 * @apiVersion 1.0.0
 * @apiName ListAllProductCategory
 * @apiGroup ProductCategory
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": [
        {
            "id": 1,
            "name": "Kilogram (kg)"
        }
    ]
}
 */

/**
 * @api {post} /api/admin/product_categories/create 2. Create ProductCategory
 * @apiVersion 1.0.0
 * @apiName CreateProductCategory
 * @apiGroup ProductCategory
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {String} name_en
 * @apiParam {String} name_kh
 * @apiParam {Integer} restaurant_id
 * @apiParam {Integer} [sequence]
 * @apiParam {Boolean} [enable_status]
 *
 @apiExample {curl} Example usage:
{
    "name_en": "Kilogram (kg)",
    "name_kh": "Kilogram (kg)",
    "restaurant_id": 1,
    "sequence": 1,
    "enable_status": 1,
}
 *
 * @apiSuccessExample  Response (example):
{
    "success": true,
    "data": null
}
 */

/**
 * @api {get} /api/admin/product_categories/show/{id} 3. Get ProductCategory detail
 * @apiVersion 1.0.0
 * @apiName GetProductCategoryDetail
 * @apiGroup ProductCategory
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "id": 1,
        "restaurant_id": 1,
        "name_en": "Kilogram (kg)",
        "name_kh": "Kilogram (kg)",
        "sequence": 1,
        "enable_status": 1,
    }
}
 */

/**
 * @api {post} /api/admin/product_categories/update/{id} 4. Update ProductCategory
 * @apiVersion 1.0.0
 * @apiName UpdateProductCategory
 * @apiGroup ProductCategory
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {String} name_en
 * @apiParam {String} name_kh
 * @apiParam {Integer} restaurant_id
 * @apiParam {Integer} [sequence]
 * @apiParam {Boolean} [enable_status]
 *
 @apiExample {curl} Example usage:
{
    "name_en": "Kilogram (kg)",
    "name_kh": "Kilogram (kg)",
    "restaurant_id": 1,
    "sequence": 1,
    "enable_status": 1,
}
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/product_categories/status/{id} 5. Update ProductCategory status
 * @apiVersion 1.0.0
 * @apiName UpdateProductCategoryStatus
 * @apiGroup ProductCategory
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {boolean} enable_status
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
 {
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/product_categories/sequence/{id} 6. Update ProductCategory sequence
 * @apiVersion 1.0.0
 * @apiName UpdateProductCategorySequence
 * @apiGroup ProductCategory
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {boolean} sequence
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
 {
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/product_categories/delete/{id} 7. Delete ProductCategory
 * @apiVersion 1.0.0
 * @apiName DeleteProductCategory
 * @apiGroup ProductCategory
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */
