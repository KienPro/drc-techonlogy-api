/**
 * @api {get} /api/admin/product_images/list 1. List ProductImage
 * @apiVersion 1.0.0
 * @apiName ListProductImage
 * @apiGroup ProductImage
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {Integer} product_id
 * @apiParam {any_string} [export]
 * @apiParam {Integer} [offset]
 * @apiParam {Integer} [limit]
 * @apiParam {String} [order]     eg. order=updated_at
 * @apiParam {String} [sort]     eg. sort=asc|desc
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "list": [
            {
                "id": 1,
                "product_id": 1,
                "media_id": 7,
                "sequence": 1,
                "deleted_at": null,
                "created_at": "2021-02-17 11:10:39",
                "updated_at": "2021-02-17 11:27:42",
                "media": {
                    "id": 7,
                    "url": "http://localhost:7081/uploads/images/c424feab597c4a28da70c7deae563391.png",
                    "name": "c424feab597c4a28da70c7deae563391.png",
                    "created_at": "2021-02-17 11:27:42",
                    "updated_at": "2021-02-17 11:27:42"
                }
            }
        ],
        "total": 1,
        "product": {
            "id": 1,
            "price": 123,
            "short_description": "Test",
            "media": {
                "id": 7,
                "url": "http://localhost:7081/uploads/images/c424feab597c4a28da70c7deae563391.png",
                "name": "c424feab597c4a28da70c7deae563391.png",
                "created_at": "2021-02-17 11:27:42",
                "updated_at": "2021-02-17 11:27:42"
            },
            "name": "Test",
            "restaurant": "Test",
            "product_category": "Kilogram (kg)"
        }
    }
}
 */

/**
 * @api {post} /api/admin/product_images/create 2. Create ProductImage
 * @apiVersion 1.0.0
 * @apiName CreateProductImage
 * @apiGroup ProductImage
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {integer} product_id
 * @apiParam {Base64Image} image
 * @apiParam {integer} [sequence]
 *
 @apiExample {curl} Example usage:
{
    "product_id": 1,
    "image": "{{image_base64}}",
    "sequence": 1
}
 *
 * @apiSuccessExample  Response (example):
{
    "success": true,
    "data": null
}
 */

/**
 * @api {get} /api/admin/product_images/show/{id} 3. Get ProductImage detail
 * @apiVersion 1.0.0
 * @apiName GetProductImageDetail
 * @apiGroup ProductImage
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "id": 1,
        "product_id": 1,
        "sequence": 1,
        "media": {
            "id": 6,
            "url": "http://localhost:7081/uploads/images/afdbdf7d6dc0927d302fbddfb75984fa.png",
            "name": "afdbdf7d6dc0927d302fbddfb75984fa.png",
            "created_at": "2021-02-17 11:10:39",
            "updated_at": "2021-02-17 11:10:39"
        }
    }
}
 */

/**
 * @api {post} /api/admin/product_images/update/{id} 4. Update ProductImage
 * @apiVersion 1.0.0
 * @apiName UpdateProductImage
 * @apiGroup ProductImage
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {Base64Image} [image]
 * @apiParam {integer} [sequence]
 *
 @apiExample {curl} Example usage:
{
    "image": "{{image_base64}}",
    "sequence": 1
}
 *
 * @apiSuccessExample  Response (example):
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/product_images/sequence/{id} 5. Update ProductImage Sequence
 * @apiVersion 1.0.0
 * @apiName UpdateProductImageSequence
 * @apiGroup ProductImage
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {integer} sequence
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/product_images/delete/{id} 6. Delete ProductImage
 * @apiVersion 1.0.0
 * @apiName DeleteProductImage
 * @apiGroup ProductImage
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */
