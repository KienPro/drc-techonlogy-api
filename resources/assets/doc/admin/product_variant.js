/**
 * @api {get} /api/admin/product_variants/list 1. List ProductVariant
 * @apiVersion 1.0.0
 * @apiName ListProductVariant
 * @apiGroup ProductVariant
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {Integer} product_id
 * @apiParam {any_string} [export]
 * @apiParam {Integer} [offset]
 * @apiParam {Integer} [limit]
 * @apiParam {String} [order]     eg. order=updated_at
 * @apiParam {String} [sort]     eg. sort=asc|desc
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "list": [
            {
                "id": 3,
                "product_id": 1,
                "name_en": "testing",
                "name_kh": "testing",
                "sequence": 1,
                "enable_status": 1,
                "deleted_at": null,
                "created_at": "2021-02-19 10:30:26",
                "updated_at": "2021-02-19 10:30:26",
                "string_values": "value testing 1, value testing 2"
            }
        ],
        "total": 2,
        "product": {
            "id": 1,
            "price": 123,
            "short_description": "Test",
            "media": {
                "id": 7,
                "url": "http://localhost:7081/uploads/images/c424feab597c4a28da70c7deae563391.png",
                "name": "c424feab597c4a28da70c7deae563391.png",
                "created_at": "2021-02-17 11:27:42",
                "updated_at": "2021-02-17 11:27:42"
            },
            "name": "Test",
            "restaurant": "Test",
            "product_category": "Kilogram (kg)"
        }
    }
}
 */

/**
 * @api {post} /api/admin/product_variants/create 2. Create ProductVariant
 * @apiVersion 1.0.0
 * @apiName CreateProductVariant
 * @apiGroup ProductVariant
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {integer} product_id
 * @apiParam {string} name_en
 * @apiParam {string} name_kh
 * @apiParam {array} values
 * @apiParam {string} values.*.name_en
 * @apiParam {string} values.*.name_kh
 * @apiParam {boolean} values.*.enable_status
 * @apiParam {integer} values.*.sequence
 * @apiParam {boolean} [enable_status]
 * @apiParam {integer} [sequence]
 *
 @apiExample {curl} Example usage:
{
    "product_id": 1,
    "name_en": "testing",
    "name_kh": "testing",
    "values": [
        {
            "name_en": "value testing",
            "name_kh": "value testing",
            "enable_status": 1,
            "sequence": 1
        },
        {
            "name_en": "value testing 2",
            "name_kh": "value testing 2",
            "enable_status": 1,
            "sequence": 1
        }
    ],
    "enable_status": 1,
    "sequence": 1
}
 *
 * @apiSuccessExample  Response (example):
{
    "success": true,
    "data": null
}
 */

/**
 * @api {get} /api/admin/product_variants/show/{id} 3. Get ProductVariant detail
 * @apiVersion 1.0.0
 * @apiName GetProductVariantDetail
 * @apiGroup ProductVariant
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "id": 3,
        "product_id": 1,
        "name_en": "testing",
        "name_kh": "testing",
        "enable_status": 1,
        "sequence": 1,
        "values": [
            {
                "id": 1,
                "name_en": "value testing 1",
                "name_kh": "value testing 1",
                "enable_status": 1,
                "sequence": 1
            },
            {
                "id": 3,
                "name_en": "value testing 2",
                "name_kh": "value testing 2",
                "enable_status": 1,
                "sequence": 1
            }
        ]
    }
}
 */

/**
 * @api {post} /api/admin/product_variants/update/{id} 4. Update ProductVariant
 * @apiVersion 1.0.0
 * @apiName UpdateProductVariant
 * @apiGroup ProductVariant
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {integer} product_id
 * @apiParam {string} name_en
 * @apiParam {string} name_kh
 * @apiParam {array} values
 * @apiParam {integer} [values.*.id]
 * @apiParam {string} values.*.name_en
 * @apiParam {string} values.*.name_kh
 * @apiParam {boolean} values.*.enable_status
 * @apiParam {integer} values.*.sequence
 * @apiParam {boolean} [enable_status]
 * @apiParam {integer} [sequence]
 *
 @apiExample {curl} Example usage:
{
    "product_id": 1,
    "name_en": "testing",
    "name_kh": "testing",
    "values": [
        {
            "id": 1,
            "name_en": "value testing 1",
            "name_kh": "value testing 1",
            "enable_status": 1,
            "sequence": 1
        },
        {
            "name_en": "value testing 2",
            "name_kh": "value testing 2",
            "enable_status": 1,
            "sequence": 1
        }
    ],
    "enable_status": 1,
    "sequence": 1
}
 *
 * @apiSuccessExample  Response (example):
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/product_variants/status/{id} 5. Update ProductVariant Status
 * @apiVersion 1.0.0
 * @apiName UpdateProductVariantStatus
 * @apiGroup ProductVariant
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {boolean} [enable_status]
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/product_variants/sequence/{id} 6. Update ProductVariant Sequence
 * @apiVersion 1.0.0
 * @apiName UpdateProductVariantSequence
 * @apiGroup ProductVariant
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {integer} sequence
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/product_variants/delete/{id} 7. Delete ProductVariant
 * @apiVersion 1.0.0
 * @apiName DeleteProductVariant
 * @apiGroup ProductVariant
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */
