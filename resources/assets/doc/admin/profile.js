/**
 * @api {get} /api/admin/auth/profile Get Profile
 * @apiVersion 1.0.0
 * @apiName GetProfile
 * @apiGroup Profile
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
 {
    "success": true,
    "data": {
        "id": 1,
        "first_name": "sokchea",
        "last_name": "van",
        "username": "vansockhea",
        "email": "admin@gmail.com",
        "gender": "Male",
        "phone_number": "0966523286",
        "address": "testing",
        "description": null,
        "enable_status": 1,
        "media_id": null,
        "created_by": null,
        "updated_by": 1,
        "deleted_at": null,
        "created_at": "2020-01-06 16:06:15",
        "updated_at": "2020-02-20 08:56:50",
        "media": {
            "id": 100,
            "media_id": 100,
            "file_url": "http://localhost:8081/uploads/images/e631fe1f9b6f7b4eb2219ec922a4190e.png",
            "file_name": "e631fe1f9b6f7b4eb2219ec922a4190e.png",
            "size": "Original"
        }
    }
}
 */

/**
 * @api {post} /api/admin/auth/profile Update Profile
 * @apiVersion 1.0.0
 * @apiName UpdateProfile
 * @apiGroup Profile
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {String} email
 * @apiParam {String} username
 * @apiParam {String} first_name
 * @apiParam {String} last_name
 * @apiParam {String} phone_number
 * @apiParam {String} [gender]
 * @apiParam {String} [address]
 * @apiParam {String} [image]   Base 64
 *
 * @apiExample {curl} Example usage:
{
	"email": "admin@gmail.com",
	"username": "vansockhea",
	"first_name": "sokchea",
	"last_name": "van",
	"phone_number": "0966523286",
	"address": "testing",
	"gender": "Male",
	"image: "Base 64.................."
}
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "id": 1,
        "first_name": "sokchea",
        "last_name": "van",
        "username": "vansockhea",
        "email": "admin@gmail.com",
        "gender": "Male",
        "phone_number": "0966523286",
        "address": "testing",
        "description": null,
        "enable_status": 1,
        "media_id": null,
        "created_by": null,
        "updated_by": 1,
        "deleted_at": null,
        "created_at": "2020-01-06 16:06:15",
        "updated_at": "2020-02-20 08:56:50",
        "media": {
            "id": 100,
            "media_id": 100,
            "file_url": "http://localhost:8081/uploads/images/e631fe1f9b6f7b4eb2219ec922a4190e.png",
            "file_name": "e631fe1f9b6f7b4eb2219ec922a4190e.png",
            "size": "Original"
        }
    }
}
 */

/**
 * @api {post} /api/operator/auth/profile/password Update Password
 * @apiVersion 1.0.0
 * @apiName UpdatePassword
 * @apiGroup Profile
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {string} old_password
 * @apiParam {string} password
 *
 * @apiExample {curl} Example usage:
{
	"old_password" : "1234567890",
	"password" : "1234567890",
}
 *
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
 {
    "success": true,
    "data": null
}
 */
