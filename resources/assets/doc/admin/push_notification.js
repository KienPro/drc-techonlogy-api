/**
 * @api {get} /api/admin/push_notifications/list 1. List Push Notification
 * @apiVersion 1.0.0
 * @apiName ListPushNotification
 * @apiGroup PushNotification
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {String} [status]          Filter by status, values 'pending' | 'sent'
 * @apiParam {String} [search]          search by 'title_en'
 * @apiParam {any_string} [export]
 * @apiParam {Integer} [offset]
 * @apiParam {Integer} [limit]
 * @apiParam {String} [order]     eg. order=updated_at
 * @apiParam {String} [sort]     eg. sort=asc|desc
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "list": [
            {
                "id": 1,
                "type_id": 1,
                "type": "supplier",
                "title_en": "Testing",
                "title_kh": "Testing",
                "message_en": "Testing",
                "message_kh": "Testing",
                "status": "pending",
                "deleted_at": null,
                "created_at": "2020-12-20 22:19:43",
                "updated_at": "2020-12-20 22:20:13"
            }
        ],
        "total": 1
    }
}
 */

/**
 * @api {post} /api/admin/push_notifications/create 2. Create Push Notification
 * @apiVersion 1.0.0
 * @apiName CreatePushNotification
 * @apiGroup PushNotification
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {Integer} type_id
 * @apiParam {String} type              valid type 'product' | 'restaurant' | 'post'
 * @apiParam {String} title_en
 * @apiParam {String} title_kh
 * @apiParam {String} message_en
 * @apiParam {String} message_kh
 *
 @apiExample {curl} Example usage:
{
    "type_id": 1,
    "type": "product",
    "title_en": "Testing",
    "title_kh": "Testing",
    "message_en": "Testing",
    "message_kh": "Testing"
}
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
 {
    "success": true,
    "data": {
        "push_notificaion_id" => 1
    }
}
 */

/**
 * @api {get} /api/admin/push_notifications/show/{id} 3. Get Push Notification detail
 * @apiVersion 1.0.0
 * @apiName GetPushNotificationDetail
 * @apiGroup PushNotification
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "id": 1,
        "type_id": 1,
        "type": "supplier",
        "title_en": "Testing",
        "title_kh": "Testing",
        "message_en": "Testing",
        "message_kh": "Testing",
        "status": "pending"
    }
}
 */

/**
 * @api {post} /api/admin/push_notifications/update/{id} 4. Update Push Notification
 * @apiVersion 1.0.0
 * @apiName UpdatePushNotification
 * @apiGroup PushNotification
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {Integer} type_id
 * @apiParam {String} type              valid type 'product' | 'restaurant' | 'post'
 * @apiParam {String} title_en
 * @apiParam {String} title_kh
 * @apiParam {String} message_en
 * @apiParam {String} message_kh
 *
 @apiExample {curl} Example usage:
{
    "type_id": 1,
    "type": "product",
    "title_en": "Testing",
    "title_kh": "Testing",
    "message_en": "Testing",
    "message_kh": "Testing"
}
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/push_notifications/send/{id} 5. Send Push Notification
 * @apiVersion 1.0.0
 * @apiName SendPushNotification
 * @apiGroup PushNotification
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
 {
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/push_notifications/delete/{id} 6. Delete Push Notification
 * @apiVersion 1.0.0
 * @apiName DeletePushNotification
 * @apiGroup PushNotification
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */
