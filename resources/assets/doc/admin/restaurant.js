/**
 * @api {get} /api/admin/restaurants/list 1. List Restaurant
 * @apiVersion 1.0.0
 * @apiName ListRestaurant
 * @apiGroup Restaurant
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {Integer} [restaurant_category_id]
 * @apiParam {Integer} [restaurant_tag_id]
 * @apiParam {Integer} [enable_status]
 * @apiParam {String} [search]          search by 'name_en' | 'name_kh'
 * @apiParam {any_string} [export]
 * @apiParam {Integer} [offset]
 * @apiParam {Integer} [limit]
 * @apiParam {String} [order]     eg. order=updated_at
 * @apiParam {String} [sort]     eg. sort=asc|desc
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "list": [
            {
                "id": 1,
                "name_en": "Test",
                "name_kh": "Test",
                "latitude": "14.12741234",
                "longitude": "104.12741234",
                "webiste_url": "fb.com",
                "email": "test@papadeliver.com",
                "phone_number": "0966523286",
                "other_phone_number": "0966523286 / 0966523286",
                "address": "Test",
                "about": "Test",
                "media_id": 6,
                "restaurant_category_id": 1,
                "amount_views": 0,
                "amount_likes": 0,
                "amount_comments": 0,
                "amount_share": 0,
                "amount_copy_link": 0,
                "amount_search": 0,
                "enable_status": 1,
                "sequence": 1,
                "is_new": 1,
                "discount": 25,
                "deleted_at": null,
                "created_at": "2021-02-01 16:29:46",
                "updated_at": "2021-02-01 16:29:46",
                "string_tags": "Kilogram (kg), Kilogram (kg) 1",
                "media": {
                    "id": 4,
                    "url": "http://localhost:7081/uploads/images/e60059995fd3c7d615e9a00ce428d1c0.png",
                    "name": "e60059995fd3c7d615e9a00ce428d1c0.png",
                    "created_at": "2021-02-16 13:23:05",
                    "updated_at": "2021-02-16 13:23:05"
                },
                "banner": {
                    "id": 5,
                    "url": "http://localhost:7081/uploads/images/660113422a106cb83620d4b11739c2bf.png",
                    "name": "660113422a106cb83620d4b11739c2bf.png",
                    "created_at": "2021-02-16 13:23:05",
                    "updated_at": "2021-02-16 13:23:05"
                },
                "restaurant_category": {
                    "id": 1,
                    "name_en": "Kilogram (kg)",
                    "name_kh": "Kilogram (kg)",
                    "type": "drink",
                    "sequence": 1,
                    "enable_status": 1,
                    "deleted_at": null,
                    "created_at": "2021-02-09 17:06:02",
                    "updated_at": "2021-02-09 17:06:02"
                }
            }
        ],
        "total": 1
    }
}
 */

/**
 * @api {get} /api/admin/restaurants/list_all 0. List All Restaurant
 * @apiVersion 1.0.0
 * @apiName ListAllRestaurant
 * @apiGroup Restaurant
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": [
        {
            "id": 1,
            "name": "Kilogram (kg)"
        }
    ]
}
 */

/**
 * @api {post} /api/admin/restaurants/create 2. Create Restaurant
 * @apiVersion 1.0.0
 * @apiName CreateRestaurant
 * @apiGroup Restaurant
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {String} name_en
 * @apiParam {String} name_kh
 * @apiParam {Float} latitude
 * @apiParam {Float} longitude
 * @apiParam {URL} webiste_url
 * @apiParam {Email} [email]
 * @apiParam {PhoneKH} phone_number
 * @apiParam {String} [other_phone_number]
 * @apiParam {String} address
 * @apiParam {Text} about
 * @apiParam {integer} restaurant_category_id
 * @apiParam {Arrat} [tag_ids]
 * @apiParam {Base64Image} image
 * @apiParam {Boolean} [enable_status]
 * @apiParam {integer} [sequence]
 * @apiParam {Boolean} [is_new]
 * @apiParam {integer} [discount]       Min:0 | Max:100
 *
 @apiExample {curl} Example usage:
{
    "name_en": "Test",
    "name_kh": "Test",
    "latitude": 14.12741234,
    "longitude": 104.12741234,
    "webiste_url": "fb.com",
    "email": "test@papadeliver.com",
    "phone_number": "0966523286",
    "other_phone_number": "0966523286 / 0966523286",
    "address": "Test",
    "about": "Test",
    "restaurant_category_id": 1,
    "enable_status": 1,
    "sequence": 1,
    "tag_ids": [1,2,3],
    "image": "{{image_base64}}",
    "is_new": 1,
    "discount": 25
}
 *
 * @apiSuccessExample  Response (example):
{
    "success": true,
    "data": null
}
 */

/**
 * @api {get} /api/admin/restaurants/show/{id} 3. Get Restaurant detail
 * @apiVersion 1.0.0
 * @apiName GetRestaurantDetail
 * @apiGroup Restaurant
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "id": 1,
        "name_en": "Test",
        "name_kh": "Test",
        "latitude": "14.12741234",
        "longitude": "104.12741234",
        "webiste_url": "fb.com",
        "email": "test@papadeliver.com",
        "phone_number": "0966523286",
        "other_phone_number": "0966523286 / 0966523286",
        "address": "Test",
        "about": "Test",
        "restaurant_category_id": 1,
        "amount_views": 0,
        "amount_likes": 0,
        "amount_comments": 0,
        "amount_share": 0,
        "amount_copy_link": 0,
        "amount_search": 0,
        "enable_status": 1,
        "sequence": 1,
        "is_new": 1,
        "discount": 25,
        "media": {
            "id": 6,
            "url": "http://localhost:8081/uploads/images/2184ed5f15e35c6e96aa5ffc7623c863.png",
            "name": "2184ed5f15e35c6e96aa5ffc7623c863.png",
            "created_at": "2021-02-01 16:29:46",
            "updated_at": "2021-02-01 16:29:46"
        },
        "banner": {
            "id": 5,
            "url": "http://localhost:7081/uploads/images/660113422a106cb83620d4b11739c2bf.png",
            "name": "660113422a106cb83620d4b11739c2bf.png",
            "created_at": "2021-02-16 13:23:05",
            "updated_at": "2021-02-16 13:23:05"
        },
        "tag_ids": [
            1,
            2,
            3
        ]
    }
}
 */

/**
 * @api {post} /api/admin/restaurants/update/{id} 4. Update Restaurant
 * @apiVersion 1.0.0
 * @apiName UpdateRestaurant
 * @apiGroup Restaurant
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {String} name_en
 * @apiParam {String} name_kh
 * @apiParam {Float} latitude
 * @apiParam {Float} longitude
 * @apiParam {URL} webiste_url
 * @apiParam {Email} [email]
 * @apiParam {PhoneKH} phone_number
 * @apiParam {String} [other_phone_number]
 * @apiParam {String} address
 * @apiParam {Text} about
 * @apiParam {integer} restaurant_category_id
 * @apiParam {Arrat} [tag_ids]
 * @apiParam {Base64Image} [image]
 * @apiParam {Boolean} [enable_status]
 * @apiParam {integer} [sequence]
 * @apiParam {Boolean} [is_new]
 * @apiParam {integer} [discount]       Min:0 | Max:100
 *
 @apiExample {curl} Example usage:
{
    "name_en": "Test",
    "name_kh": "Test",
    "latitude": 14.12741234,
    "longitude": 104.12741234,
    "webiste_url": "fb.com",
    "email": "test@papadeliver.com",
    "phone_number": "0966523286",
    "other_phone_number": "0966523286 / 0966523286",
    "address": "Test",
    "about": "Test",
    "restaurant_category_id": 1,
    "enable_status": 1,
    "sequence": 1,
    "tag_ids": [1,2,3],
    "image": "{{image_base64}}",
    "is_new": 1,
    "discount": 25
}
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/restaurants/status/{id} 5. Update Restaurant status
 * @apiVersion 1.0.0
 * @apiName UpdateRestaurantStatus
 * @apiGroup Restaurant
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {boolean} enable_status
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
 {
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/restaurants/sequence/{id} 6. Update Restaurant Sequence
 * @apiVersion 1.0.0
 * @apiName UpdateRestaurantSequence
 * @apiGroup Restaurant
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {integer} sequence
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/restaurants/delete/{id} 7. Delete Restaurant
 * @apiVersion 1.0.0
 * @apiName DeleteRestaurant
 * @apiGroup Restaurant
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */
