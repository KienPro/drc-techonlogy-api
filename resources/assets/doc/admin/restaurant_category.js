/**
 * @api {get} /api/admin/restaurant_categories/list 1. List RestaurantCategory
 * @apiVersion 1.0.0
 * @apiName ListRestaurantCategory
 * @apiGroup RestaurantCategory
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {String} [search]          search by 'name_en' | 'name_kh'
 * @apiParam {String} [type]            values in 'drink' 'fast_food' 'restaurant'
 * @apiParam {any_string} [export]
 * @apiParam {Integer} [offset]
 * @apiParam {Integer} [limit]
 * @apiParam {String} [order]     eg. order=updated_at
 * @apiParam {String} [sort]     eg. sort=asc|desc
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "list": [
            {
                "id": 1,
                "name_en": "Kilogram (kg)",
                "name_kh": "Kilogram (kg)",
                "tyoe": "drink",
                "sequence": 2,
                "enable_status": 1,
                "deleted_at": null,
                "created_at": "2021-01-31 22:54:26",
                "updated_at": "2021-01-31 22:54:26"
            }
        ],
        "total": 1
    }
}
 */

/**
 * @api {get} /api/admin/restaurant_categories/list_all 0. List All RestaurantCategory
 * @apiVersion 1.0.0
 * @apiName ListAllRestaurantCategory
 * @apiGroup RestaurantCategory
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": [
        {
            "id": 1,
            "name": "Kilogram (kg) (drink)"
        }
    ]
}
 */

/**
 * @api {post} /api/admin/restaurant_categories/create 2. Create RestaurantCategory
 * @apiVersion 1.0.0
 * @apiName CreateRestaurantCategory
 * @apiGroup RestaurantCategory
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {String} name_en
 * @apiParam {String} name_kh
 * @apiParam {String} [type]            values in 'drink' 'fast_food' 'restaurant'
 * @apiParam {Integer} [sequence]
 * @apiParam {Boolean} [enable_status]
 *
 @apiExample {curl} Example usage:
{
    "name_en": "Kilogram (kg)",
    "name_kh": "Kilogram (kg)",
    "tyep": "drink",
    "sequence": 1,
    "enable_status": 1,
}
 *
 * @apiSuccessExample  Response (example):
{
    "success": true,
    "data": null
}
 */

/**
 * @api {get} /api/admin/restaurant_categories/show/{id} 3. Get RestaurantCategory detail
 * @apiVersion 1.0.0
 * @apiName GetRestaurantCategoryDetail
 * @apiGroup RestaurantCategory
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "id": 1,
        "name_en": "Kilogram (kg)",
        "name_kh": "Kilogram (kg)",
        "tyep": "drink",
        "sequence": 1,
        "enable_status": 1,
    }
}
 */

/**
 * @api {post} /api/admin/restaurant_categories/update/{id} 4. Update RestaurantCategory
 * @apiVersion 1.0.0
 * @apiName UpdateRestaurantCategory
 * @apiGroup RestaurantCategory
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {String} name_en
 * @apiParam {String} name_kh
 * @apiParam {String} [type]            values in 'drink' 'fast_food' 'restaurant'
 * @apiParam {Integer} [sequence]
 * @apiParam {Boolean} [enable_status]
 *
 @apiExample {curl} Example usage:
{
    "name_en": "Kilogram (kg)",
    "name_kh": "Kilogram (kg)",
    "tyep": "drink",
    "sequence": 1,
    "enable_status": 1,
}
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/restaurant_categories/status/{id} 5. Update RestaurantCategory status
 * @apiVersion 1.0.0
 * @apiName UpdateRestaurantCategoryStatus
 * @apiGroup RestaurantCategory
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {boolean} enable_status
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
 {
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/restaurant_categories/sequence/{id} 6. Update RestaurantCategory sequence
 * @apiVersion 1.0.0
 * @apiName UpdateRestaurantCategorySequence
 * @apiGroup RestaurantCategory
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {boolean} sequence
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
 {
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/restaurant_categories/delete/{id} 7. Delete RestaurantCategory
 * @apiVersion 1.0.0
 * @apiName DeleteRestaurantCategory
 * @apiGroup RestaurantCategory
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */
