/**
 * @api {get} /api/admin/restaurant_tags/list 1. List RestaurantTag
 * @apiVersion 1.0.0
 * @apiName ListRestaurantTag
 * @apiGroup RestaurantTag
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {String} [search]          search by 'name_en' | 'name_kh'
 * @apiParam {any_string} [export]
 * @apiParam {Integer} [offset]
 * @apiParam {Integer} [limit]
 * @apiParam {String} [order]     eg. order=updated_at
 * @apiParam {String} [sort]     eg. sort=asc|desc
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "list": [
            {
                "id": 1,
                "name_en": "Kilogram (kg)",
                "name_kh": "Kilogram (kg)",
                "media_id": null,
                "enable_status": 1,
                "sequence": 1,
                "deleted_at": null,
                "created_at": "2021-02-01 14:13:43",
                "updated_at": "2021-02-01 14:13:43"
            }
        ],
        "total": 1
    }
}
 */

/**
 * @api {get} /api/admin/restaurant_tags/list_all 0. List All RestaurantTag
 * @apiVersion 1.0.0
 * @apiName ListAllRestaurantTag
 * @apiGroup RestaurantTag
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": [
        {
            "id": 1,
            "name": "Kilogram (kg)"
        }
    ]
}
 */

/**
 * @api {post} /api/admin/restaurant_tags/create 2. Create RestaurantTag
 * @apiVersion 1.0.0
 * @apiName CreateRestaurantTag
 * @apiGroup RestaurantTag
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {String} name_en
 * @apiParam {String} name_kh
 * @apiParam {Boolean} [enable_status]
 * @apiParam {integer} [sequence]
 *
 @apiExample {curl} Example usage:
{
    "name_en": "Kilogram (kg)",
    "name_kh": "Kilogram (kg)",
    "enable_status": 1,
    "sequence": 1
}
 *
 * @apiSuccessExample  Response (example):
{
    "success": true,
    "data": null
}
 */

/**
 * @api {get} /api/admin/restaurant_tags/show/{id} 3. Get RestaurantTag detail
 * @apiVersion 1.0.0
 * @apiName GetRestaurantTagDetail
 * @apiGroup RestaurantTag
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "id": 1,
        "name_en": "Kilogram (kg)",
        "name_kh": "Kilogram (kg)",
        "enable_status": 1,
        "sequence": 1
    }
}
 */

/**
 * @api {post} /api/admin/restaurant_tags/update/{id} 4. Update RestaurantTag
 * @apiVersion 1.0.0
 * @apiName UpdateRestaurantTag
 * @apiGroup RestaurantTag
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {String} name_en
 * @apiParam {String} name_kh
 * @apiParam {Boolean} [enable_status]
 * @apiParam {integer} [sequence]
 *
 @apiExample {curl} Example usage:
{
    "name_en": "Kilogram (kg)",
    "name_kh": "Kilogram (kg)",
    "enable_status": 1,
    "sequence": 1
}
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/restaurant_tags/status/{id} 5. Update RestaurantTag status
 * @apiVersion 1.0.0
 * @apiName UpdateRestaurantTagStatus
 * @apiGroup RestaurantTag
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {boolean} enable_status
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
 {
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/restaurant_tags/sequence/{id} 6. Update RestaurantTag Sequence
 * @apiVersion 1.0.0
 * @apiName UpdateRestaurantTagSequence
 * @apiGroup RestaurantTag
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {integer} sequence
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/restaurant_tags/delete/{id} 7. Delete RestaurantTag
 * @apiVersion 1.0.0
 * @apiName DeleteRestaurantTag
 * @apiGroup RestaurantTag
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */
