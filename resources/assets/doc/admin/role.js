/**
 * @api {get} /api/admin/roles/list 1. List Roles
 * @apiVersion 1.0.0
 * @apiName ListRoles
 * @apiGroup Roles
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {integer} [enable_status]
 * @apiParam {String} [search]      search fields = "name"
 * @apiParam {any_string} [export]
 * @apiParam {Integer} [offset]
 * @apiParam {Integer} [limit]
 * @apiParam {String} [order]     eg. order=updated_at
 * @apiParam {String} [sort]     eg. sort=asc|desc
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "list": [
            {
                "id": 1,
                "name": "Admin",
                "description": "Admin",
                "enable_status": 1,
                "created_by": null,
                "updated_by": null,
                "deleted_at": null,
                "created_at": "2020-04-06 17:32:37",
                "updated_at": "2020-04-06 17:32:37"
            }
        ],
        "total": 1
    }
}
 */

/**
 * @api {get} /api/admin/roles/list_all 0. List All Roles
 * @apiVersion 1.0.0
 * @apiName ListAllRoles
 * @apiGroup Roles
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": [
        {
            "id": 1,
            "name": "Admin"
        }
    ]
}
 */

/**
 * @api {post} /api/admin/roles/create 2. Create Roles
 * @apiVersion 1.0.0
 * @apiName CreateRoles
 * @apiGroup Roles
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {String} [name]
 * @apiParam {String} [description]
 * @apiParam {boolean} [enable_status]
 *
 @apiExample {curl} Example usage:
{
	"name": "Admin",
	"description": "Admin",
	"enable_status": 1,
    "permissions": [
        1,
        2
    ]
}
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {get} /api/admin/roles/show/{id} 3. Get Roles detail
 * @apiVersion 1.0.0
 * @apiName GetRolesDetail
 * @apiGroup Roles
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "id": 1,
        "name": "Admin",
        "description": "Admin",
        "enable_status": 1,
        "permissions": [
            1,
            2
        ]
    }
}
 */

/**
 * @api {post} /api/admin/roles/update/{id} 4. Update Roles
 * @apiVersion 1.0.0
 * @apiName UpdateRoles
 * @apiGroup Roles
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {String} [name]
 * @apiParam {String} [description]
 * @apiParam {boolean} [enable_status]
 *
 @apiExample {curl} Example usage:
{
	"name": "Admin",
	"description": "Admin",
	"enable_status": 1,
    "permissions": [
        1,
        2
    ]
}
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/roles/status/{id} 5. Update Roles status
 * @apiVersion 1.0.0
 * @apiName UpdateRolesStatus
 * @apiGroup Roles
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {boolean} enable_status
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/roles/delete/{id} 6. Delete Roles
 * @apiVersion 1.0.0
 * @apiName DeleteRoles
 * @apiGroup Roles
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */
