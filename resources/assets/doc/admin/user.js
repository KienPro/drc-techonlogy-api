/**
 * @api {get} /api/admin/users/list 1. List User
 * @apiVersion 1.0.0
 * @apiName ListUser
 * @apiGroup User
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {String} [search]
 * @apiParam {any_string} [export]
 * @apiParam {Integer} [offset]
 * @apiParam {Integer} [limit]
 * @apiParam {String} [order]     eg. order=updated_at
 * @apiParam {String} [sort]     eg. sort=asc|desc
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
 {
    "success": true,
    "data": {
        "list": [
            "id": 16,
            "first_name": null,
            "last_name": null,
            "username": "saxez",
            "email": "buwefyl@mailinator.net",
            "gender": "male",
            "phone_number": "0123456789",
            "address": null,
            "description": "Elit nemo eos et l",
            "enable_status": 1,
            "media_id": 2,
            "created_by": 1,
            "updated_by": null,
            "deleted_at": null,
            "created_at": "2020-01-03 14:44:10",
            "updated_at": "2020-01-03 15:00:42",
            "roles": [
                {
                    "id": 1,
                    "name": "Admin"
                }
            ]
        ],
        "total": 1
    }
}
 */

/**
 * @api {get} /api/admin/users/list_all 0. List All User
 * @apiVersion 1.0.0
 * @apiName ListAllUser
 * @apiGroup User
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 *
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": [
        {
            "id": 1,
            "name": "A001"
        }
    ]
}
 */

/**
 * @api {post} /api/admin/users/create 2. Create User
 * @apiVersion 1.0.0
 * @apiName CreateUser
 * @apiGroup User
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {String} first_name
 * @apiParam {String} last_name
 * @apiParam {String} gender
 * @apiParam {String} username
 * @apiParam {String} email
 * @apiParam {String} password
 * @apiParam {String} phone
 * @apiParam {array} roles
 * @apiParam {String} [address]
 * @apiParam {String} [description]
 *
@apiExample {curl} Example usage:
{
	"first_name": "test",
	"last_name": "dev",
    "username": "devtest015",
    "gender": "male",
	"email": "devtest015@gmail.com",
	"password": "12346567890",
	"phone_number": "+85512838448",
	"address": "Phnom Penh",
	"description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
	"roles" : [
	    1
	 ],
	 "enable_status": 1
}
 *
 * @apiSuccessExample  Response (example):
HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/users/update/{id} 4. Update User
 * @apiVersion 1.0.0
 * @apiName UpdateUser
 * @apiGroup User
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {String} first_name
 * @apiParam {String} last_name
 * @apiParam {String} gender
 * @apiParam {String} username
 * @apiParam {String} email
 * @apiParam {String} password
 * @apiParam {String} phone
 * @apiParam {Array} roles
 * @apiParam {String} [address]
 * @apiParam {String} [description]
 *
 @apiExample {curl} Example usage:
{
	"first_name": "test",
	"last_name": "dev",
    "user_name": "devtest1",
    "gender": "Male",
	"email": "devtest0123@gmail.com",
	"phone_number": "+85512838448",
	"address": "Phnom Penh",
	"description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
	"roles" : [
	    1
	 ],
	 "enable_status": 0
}
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {post} /api/admin/users/delete/{id} 6. Delete User
 * @apiVersion 1.0.0
 * @apiName DeleteUser
 * @apiGroup User
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": null
}
 */

/**
 * @api {get} /api/admin/users/show/{id} 3. Get User detail
 * @apiVersion 1.0.0
 * @apiName GetUserDetail
 * @apiGroup User
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
{
    "success": true,
    "data": {
        "id": 15,
        "username": "wajoni",
        "first_name": null,
        "last_name": null,
        "address": null,
        "description": "Aspernatur impedit",
        "email": "rugy@mailinator.net",
        "enable_status": 0,
        "gender": "female",
        "phone_number": "0123456789",
        "media": {
            "id": 1,
            "media_id": 1,
            "file_url": "http://localhost:8081/uploads/images/251d84509dadf8ee5059a35a472850a9.png",
            "file_name": "251d84509dadf8ee5059a35a472850a9.png",
            "size": "Original"
        },
        "roles": [
            1
        ]
    }
}
 */

/**
 * @api {post} /api/admin/users/status/{id} 5. Update User status
 * @apiVersion 1.0.0
 * @apiName UpdateUserStatus
 * @apiGroup User
 *
 * @apiHeader {String} Authorization Access token. Ex: 'Bearer ' + token
 * @apiHeader {String} Accept 'application/json'
 *
 * @apiParam {boolean} enable_status
 *
 * @apiSuccessExample  Response (example):
 HTTP/1.1 200 Success Request
 {
    "success": true,
    "data": null
}
 */
