<?php

return [
    'not_found' => ':attribute not found.',
    'is_deleted' => ':attribute is has been deleted',
    'invalid' => [
        'verify_code' => 'Invalid verify code',
        'phone_number_or_password' => 'Invalid phone number or password.',
        'phone_number' => 'Invalid phone number.',
        'old_password' => 'Your old password is incorrect.',
    ],
    'can_not' => [
        'reset_password' => 'Can not Reset Password.'
    ],
    'reset_password_successfully' => 'Reset password successfully.',
    'verification_message' => ':code is your verification code.',
];

