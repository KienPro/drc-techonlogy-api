<?php

return [
    'not_found' => ':attribute​គឺមិនមានទេ',
    'is_deleted' => ':attribute​គឺបានលុប',
    'invalid' => [
        'verify_code' => 'លេខកូដមិនត្រឹមតូ្រវ',
        'phone_number_or_password' => 'លេខទូរស័ព្ទ ឬ ពាក្យសម្ងាត់មិនត្រឹមត្រូវ',
        'phone_number' => 'លេខទូរស័ព្ទមិនត្រឹមត្រូវ',
        'old_password' => 'ពាក្យសម្ងាត់ចាស់របស់អ្នកមិនត្រឹមត្រូវ',
    ],
    'can_not' => [
        'reset_password' => 'មិនអាចប្តូរពាក្យសម្ងាត់',
    ],
    'reset_password_successfully' => 'កំណត់ពាក្យសម្ងាត់ឡើងវិញដោយជោគជ័យ',
    'verification_message' => ':code គឺជាលេខកូដផ្ទៀងផ្ទាត់របស់អ្នក',
];
