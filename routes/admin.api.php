<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', 'AuthController@login');
    Route::post('/logout', 'AuthController@logout');

    Route::group(['prefix' => 'forgot_password'], function () {
        Route::post('/', 'AuthController@forgotPassword');
        Route::post('/verify', 'AuthController@forgotPasswordVerify');
        Route::post('/reset', 'AuthController@forgotPasswordReset');
    });

    Route::group(['prefix' => 'profile', 'middleware' => ['auth:sanctum', 'multi_auth:web']], function () {
        Route::get('/', 'ProfileController@get');
        Route::post('/', 'ProfileController@update');
        Route::post('/password', 'ProfileController@updatePassword');
    });

    Route::get('/user_per', 'AuthController@getUserPer')->middleware(['auth:sanctum', 'multi_auth:web']);
});

Route::group(['middleware' => ['auth:sanctum', 'multi_auth:web']], function () {

    // Permissions
    Route::group(['prefix' => 'permissions'], function () {
        Route::get('/list_all', 'PermissionController@listAll');
    });

    // Roles
    Route::group(['prefix' => 'roles'], function () {
        Route::get('/list', 'RoleController@list');
        Route::get('/list/all', 'RoleController@listAll');
        Route::get('/show/{id}', 'RoleController@show');
        Route::post('/create', 'RoleController@create');
        Route::post('/update/{id}', 'RoleController@update');
        Route::post('/status/{id}', 'UserController@updateStatus');
        Route::post('/delete/{id}', 'RoleController@destroy');
    });

    // Users
    Route::group(['prefix' => 'users'], function () {
        Route::get('/list', 'UserController@list');
        Route::get('/list_all', 'UserController@listAll');
        Route::get('/show/{id}', 'UserController@show');
        Route::post('/create', 'UserController@create');
        Route::post('/update/{id}', 'UserController@update');
        Route::post('/status/{id}', 'UserController@updateStatus');
        Route::post('/delete/{id}', 'UserController@delete');
    });

    // Customers
    Route::group(['prefix' => 'customers'], function () {
        Route::get('/list', 'CustomerController@list');
        Route::get('/list_all', 'CustomerController@listAll');
        Route::post('/create', 'CustomerController@create');
        Route::get('/show/{id}', 'CustomerController@show');
        Route::post('/update/{id}', 'CustomerController@update');
        Route::post('/status/{id}', 'CustomerController@status');
        Route::post('/delete/{id}', 'CustomerController@delete');
    });

    // Dashbard
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('/', 'DashboardController@index');
    });

    // Product Categories
    Route::group(['prefix' => 'product_categories'], function () {
        Route::get('/list', 'ProductCategoryController@list');
        Route::get('/list_all', 'ProductCategoryController@listAll');
        Route::post('/create', 'ProductCategoryController@create');
        Route::get('/show/{id}', 'ProductCategoryController@show');
        Route::post('/update/{id}', 'ProductCategoryController@update');
        Route::post('/delete/{id}', 'ProductCategoryController@delete');
    });

    // Product Models
    Route::group(['prefix' => 'product_models'], function () {
        Route::get('/list', 'ProductModelController@list');
        Route::get('/list_all', 'ProductModelController@listAll');
        Route::post('/create', 'ProductModelController@create');
        Route::get('/show/{id}', 'ProductModelController@show');
        Route::post('/update/{id}', 'ProductModelController@update');
        Route::post('/delete/{id}', 'ProductModelController@delete');
    });

    // Product
    Route::group(['prefix' => 'products'], function () {
        Route::get('/list', 'ProductController@list');
        Route::get('/list_all', 'ProductController@listAll');
        Route::post('/create', 'ProductController@create');
        Route::get('/show/{id}', 'ProductController@show');
        Route::post('/update/{id}', 'ProductController@update');
        Route::post('/delete/{id}', 'ProductController@delete');
        Route::post('/status/bestseller/{id}', 'ProductController@status');
    });

    // Product Image
    Route::group(['prefix' => 'product_images'], function () {
        Route::get('/list', 'ProductImageController@list');
        Route::post('/create', 'ProductImageController@store');
        Route::get('/show/{id}', 'ProductImageController@show');
        Route::post('/update/{id}', 'ProductImageController@update');
        Route::post('/sequence/{id}', 'ProductImageController@status');
        Route::post('/delete/{id}', 'ProductImageController@delete');
    });

    // Slideshow
    Route::group(['prefix' => 'slideshows'], function () {
        Route::get('/list', 'SlideshowController@list');
        Route::post('/create', 'SlideshowController@store');
        Route::get('/show/{id}', 'SlideshowController@show');
        Route::post('/update/{id}', 'SlideshowController@update');
        Route::post('/delete/{id}', 'SlideshowController@delete');
    });

    // Product Tag
    Route::group(['prefix' => 'product_tags'], function () {
        Route::get('/list', 'ProductTagController@list');
        Route::get('/list_all', 'ProductTagController@listAll');
        Route::post('/create', 'ProductTagController@create');
        Route::get('/show/{id}', 'ProductTagController@show');
        Route::post('/update/{id}', 'ProductTagController@update');
        Route::post('/delete/{id}', 'ProductTagController@delete');
    });

    // View Order
    Route::group(['prefix' => 'view_orders'], function () {
        Route::get('/list', 'ViewOrderController@list');
        Route::get('/count/order', 'ViewOrderController@count');     
        Route::post('/update/{id}', 'ViewOrderController@update');
        Route::get('/invoice/{code}', 'ViewOrderController@invoice');
        Route::post('/payment/{id}', 'ViewOrderController@payment');
        Route::post('/product/delete/{id}', 'ViewOrderController@removeProduct');
    });

    //Report
    Route::group(['prefix' => 'reports'], function () {
        Route::get('/customer', 'ReportController@customer');
        Route::get('/sale', 'ReportController@sale');
        Route::get('/payment', 'ReportController@payment');
    });


});
