<?php

use Illuminate\Support\Facades\Route;

// Product
Route::group(['prefix' => 'slideshows'], function () {
    Route::get('/list', 'SlideshowController@list');
});

// Product
Route::group(['prefix' => 'products'], function () {
    Route::get('/list', 'ProductController@list');
    Route::get('/quickview/{id}', 'ProductController@quickview');
    Route::get('/details/{id}', 'ProductController@details');
    Route::get('/compare/{id}', 'ProductController@compare');
    
});

//Prouduct Category
Route::group(['prefix' => 'product_categories'], function () {
    Route::get('/list', 'ProductCategoryController@list');
});

//Prouduct Category
Route::group(['prefix' => 'product_tags'], function () {
    Route::get('/list', 'ProductTagsController@list');
});

//Prouduct Model
Route::group(['prefix' => 'product_models'], function () {
    Route::get('/list', 'ProductModelController@list');
});

Route::group(['prefix' => 'auth'], function () {
    Route::post('/register', 'AuthController@register');

    Route::post('/login', 'AuthController@login');
    Route::post('/logout', 'AuthController@logout');

    Route::group(['prefix' => 'forgot_password'], function () {
        Route::post('/', 'AuthController@forgotPassword');
        Route::post('/verify', 'AuthController@forgotPasswordVerify');
        Route::post('/reset', 'AuthController@forgotPasswordReset');
    });

    Route::group(['prefix' => 'profile', 'middleware' => ['auth:sanctum', 'multi_auth:customers']], function () {
        Route::get('/', 'ProfileController@get');
        Route::post('/', 'ProfileController@update');
        Route::post('/password', 'ProfileController@updatePassword');
    });

    // Route::get('/user_per', 'AuthController@getUserPer')->middleware(['auth:sanctum', 'multi_auth:web']);
});

Route::group(['middleware' => ['auth:sanctum', 'multi_auth:customers']], function() {
    Route::group(['prefix' => 'wishlists'], function () {
        Route::get('/list', 'WishlistController@list');
        Route::post('/wish', 'WishlistController@wish');
        Route::delete('/unwish/{id}', 'WishlistController@unwish');
        Route::get('/count/wishlist', 'WishlistController@count');     
    });

    Route::group(['prefix' => 'carts'], function () {
        Route::get('/list', 'CartController@list');
        Route::post('/add/single_cart', 'CartController@singleCart');
        Route::post('/add/multi_cart', 'CartController@multipleCart');
        Route::get('/count/cart', 'CartController@count');  
        Route::post('/delete/{id}', 'CartController@delete');
    });
    
    Route::group(['prefix' => 'orders'], function () {
        Route::post('/create', 'OrderController@store');
    });
});

